#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
class A {
private:
	int a = 0;
public:
	A() {
		a = 10;
	}
	void printf() {
		cout << "aa" << endl;
	}
	~A() {
		cout << "~A()" << endl;
	}
};
void test() {
	A* p = new A;
	int* mp = (int*)malloc(sizeof(int));
	*mp = 100;
	delete p;
	free(mp);
}

void test2() {
	string str1 = "asd";
	string str2 = "sad";
	string str3 = "das";

	cout << int('a' + 's' + 'd') << endl;
	cout << int('s' + 'a' + 'd') << endl;
}
int main() {
	//test();
	test2();
	return 0;
}

