#pragma once
#include<iostream>
namespace zhu {
	enum colour {
		RED,
		BLACK
	};

	template<class T>
	struct RBTreeNode {
		RBTreeNode* _left;
		RBTreeNode* _right;
		RBTreeNode* _parent;
		colour _col;
		T _data;

		RBTreeNode(const T& data)
			: _left(nullptr)
			, _right(nullptr)
			, _parent(nullptr)
			, _col(RED)
			, _data(data)
		{}
	};
	template<class T, class Ref, class Ptr>
	class RBTreeIterator {
		typedef RBTreeNode<T> Node;
		typedef RBTreeIterator<T, Ref, Ptr> self;
	private:
		Node* _node;
	public:
		RBTreeIterator(Node* node)
			:_node(node)
		{}
		~RBTreeIterator(){
			_node = nullptr;
		}
		Ref operator*() {
			return _node->_data;
		}
		Ptr operator->() {
			return &_node->_data;
		}
		bool operator!=(const self& s) {
			return _node != s._node;
		}
		self& operator++() {
			if (_node->_right) {
				Node* min = _node->_right;
				while (min->_left) {
					min = min->_left;
				}
				_node = min;
			}
			else {
				Node* cur = _node;
				Node* parent = cur->_parent;
				while (parent&& cur == parent->_right) {
					cur = parent;
					parent = parent->_parent;
				}
				_node = parent;
			}
			return *this;
		}
		self& operator--() {
			if (_node->_left) {
				Node* max = _node->_left;
				while (max->_right) {
					max = max->_right;
				}
				_node = max;
			}
			else {
				Node* cur = _node;
				Node* parent = cur->_parent;
				while (parent && cur == parent->_left) {
					cur = parent;
					parent = parent->_parent;
				}
				_node = parent;
			}
			return *this;
		}
	};
	//set RBTree<class K, K, SetKeyOfT>
	//map RBTree<class K, pair<K, V>, MapKeyOfT>
	template<class K, class T, class KeyOfT>
	//K这个参数也是需要的不然无法知道key类型
	class RBTree {
		KeyOfT kot;
		typedef RBTreeNode<T> Node;
	private:
		Node* _root;
	public:
		typedef RBTreeIterator<T, T&, T*> iterator;

		RBTree()
			:_root(nullptr)
		{}
		~RBTree() {
			Destory(_root);
			_root = nullptr;
		}
		RBTree(const RBTree<K, T, KeyOfT>& rbt) {
			_root = Copy(rbt._root);
		}
		RBTree<K, T, KeyOfT>& operator=(RBTree<K, T, KeyOfT> rbt) {
			swap(_root, rbt._root);
			return *this;
		}
		iterator begin() {
			Node* Rleft = _root;
			while (Rleft && Rleft->_left) {
				Rleft = Rleft->_left;
			}
			return iterator(Rleft);
		}
		iterator end() {
			return iterator(nullptr);
		}
		iterator find(const K& key) {
			Node* cur = _root;
			while (cur) {
				if (kot(cur->_data) > key) {
					cur = cur->_left;
				}
				else if (kot(cur->_data) < key) {
					cur = cur->_right;
				}
				else {
					return iterator(cur);
				}
			}
			return end();
		}
		std::pair<iterator, bool> Insert(const T& data) {
			if (_root == nullptr) {
				_root = new Node(data);
				_root->_col = BLACK;
				return std::make_pair(iterator(_root), true);
			}
			//先像搜索树一样进行插入
			Node* cur = _root;
			Node* parent = nullptr;
			while (cur) {
				if (kot(cur->_data) > kot(data)) {
					parent = cur;
					cur = cur->_left;
				}
				else if (kot(cur->_data) < kot(data)) {
					parent = cur;
					cur = cur->_right;
				}
				else {
					return std::make_pair(iterator(cur), false);
				}
			}
			cur = new Node(data);
			Node* newNode = cur;
			if (kot(parent->_data) < kot(cur->_data)) {
				parent->_right = cur;
				cur->_parent = parent;
			}
			else if (kot(parent->_data) > kot(cur->_data)) {
				parent->_left = cur;
				cur->_parent = parent;
			}

			//在控制平衡
			while (parent && parent->_col == RED) {
				Node* grandparent = parent->_parent;
				if (parent == grandparent->_left) {
					Node* uncle = grandparent->_right;
					//叔叔存在且为红，调色+继续向上处理
					if (uncle && uncle->_col == RED) {
						parent->_col = BLACK;
						uncle->_col = BLACK;
						grandparent->_col = RED;
						cur = grandparent;
						parent = cur->_parent;
					}
					else { //叔叔不存在,或叔叔存在且为黑 --> 旋转 + 变色
						if (cur == parent->_left) { //直线单旋处理 --> 情况二
							//右单旋
							RotateR(grandparent);
							grandparent->_col = RED;
							parent->_col = BLACK;
						}
						else { //折线双旋处理  --> 情况三
							RotateL(parent);
							RotateR(grandparent);
							grandparent->_col = RED;
							parent->_col = BLACK;
						}
						break;
					}
				}
				else if (parent == grandparent->_right) {
					Node* uncle = grandparent->_left;
					//叔叔存在且为红，调色+继续向上处理
					if (uncle && uncle->_col == RED) {
						parent->_col = BLACK;
						uncle->_col = BLACK;
						grandparent->_col = RED;
						cur = grandparent;
						parent = cur->_parent;
					}
					else { //叔叔不存在,或叔叔存在且为黑 --> 旋转 + 变色
						if (cur == parent->_right) { //直线单旋处理 --> 情况二
							//右单旋
							RotateL(grandparent);
							grandparent->_col = RED;
							parent->_col = BLACK;
						}
						else { //折线双旋处理  --> 情况三
							RotateR(parent);
							RotateL(grandparent);
							grandparent->_col = RED;
							parent->_col = BLACK;
						}
						break;
					}
				}
			}
			_root->_col = BLACK;
			return std::make_pair(iterator(newNode), true);
		}
		Node* GetRoot() {
			return _root;
		}
		void Inorder() {
			_Inorder(_root);
		}
		bool IsBalance() {
			if (_root->_col == RED) {
				return false;
			}
			int blackNum = 0;
			//定义一条路径黑色节点数量为参考值
			int benchmark = 0;
			Node* left = _root;
			while (left) {
				if (left->_col == BLACK) {
					benchmark++;
				}
				left = left->_left;
			}
			return _IsBalance(_root, benchmark, blackNum);
		}
	private:

		void RotateL(Node* parent) {
			Node* subR = parent->_right;
			Node* subRL = subR->_left;

			parent->_right = subRL;
			if (subRL)
				subRL->_parent = parent;

			Node* grandparent = parent->_parent;
			subR->_left = parent;
			parent->_parent = subR;

			if (parent == _root) {
				_root = subR;
				_root->_parent = nullptr;
			}
			else {
				if (parent == grandparent->_left) {
					grandparent->_left = subR;
				}
				else if (parent == grandparent->_right) {
					grandparent->_right = subR;
				}
				subR->_parent = grandparent;
			}
		}
		void RotateR(Node* parent) {
			Node* subL = parent->_left;
			Node* subLR = subL->_right;

			parent->_left = subLR;
			if (subLR)
				subLR->_parent = parent;
			Node* grandparent = parent->_parent;
			parent->_parent = subL;
			subL->_right = parent;

			if (parent == _root) {
				_root = subL;
				_root->_parent = nullptr;
			}
			else {
				if (parent == grandparent->_left) {
					grandparent->_left = subL;
				}
				else {
					grandparent->_right = subL;
				}
				subL->_parent = grandparent;
			}
		}
		void _Inorder(Node* root) {
			if (root == nullptr) {
				return;
			}
			_Inorder(root->_left);
			std::cout << kot(root->_data) << std::endl;
			//std::cout << root->_kv.first << " : " << root->_kv.second << std::endl;
			_Inorder(root->_right);
		}
		int _Height(Node* root) {
			if (root == nullptr) {
				return 0;
			}
			int leftHeight = _Height(root->_left);
			int rightHeight = _Height(root->_right);

			return leftHeight > rightHeight ? leftHeight + 1 : rightHeight + 1;
		}
		bool _IsBalance(Node* root, int benchmark, int blackNum) {
			//从红黑树性质入手
			if (root == nullptr) {
				if (benchmark != blackNum){
					std::cout << "存在路径黑色节点不相等" << std::endl;
					return false;
				}
				return true;
			}
			if (root->_col == RED && root->_parent->_col == RED) {
				std::cout << "存在连续红色节点" << std::endl;
				return false;
			}
			if (root->_col == BLACK) {
				blackNum++;
			}
			return _IsBalance(root->_left, benchmark, blackNum)
				&& _IsBalance(root->_right, benchmark, blackNum);
		}
		void Destory(Node* root) {
			if (root == nullptr)
				return;
			Destory(root->_left);
			Destory(root->_right);
			delete root;
		}
		Node* Copy(Node* root) {
			if (root == nullptr) {
				return nullptr;
			}
			Node* newNode = new Node(root->_data);
			newNode->_col = root->_col;

			newNode->_left = Copy(root->_left);
			newNode->_right = Copy(root->_right);
			if (newNode->_left)
				newNode->_left->_parent = newNode;
			if (newNode->_right)
				newNode->_right->_parent = newNode;
		}
	};

	//void test_RBTree() {
		//RBTree<int, int> t1;
		//t1.Insert({ 1, 1 });
		//t1.Insert({ 2, 1 });
		//t1.Insert({ 3, 1 });
		//t1.Insert({ 4, 1 });
		//t1.Insert({ 8, 1 });
		//t1.Insert({ 9, 1 });
		//t1.Insert({ 6, 1 });
		//t1.Inorder();
		//std::cout << t1.IsBalance() << std::endl;
	//}
}









