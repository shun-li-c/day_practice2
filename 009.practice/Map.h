#pragma once
#include"RBTree.h"
namespace zhu {
	template<class K, class V>
	class map {
		struct MapKeyOfT {
			const K& operator()(const std::pair<K, V>& KV) {
				return KV.first;
			}
		};
		typedef RBTreeNode<std::pair<K, V>> Node;
	public:
		typedef typename RBTree<K, std::pair<K, V>, MapKeyOfT>::iterator iterator;
		//类模板没实例化前什么都不是,typename告诉编译器后面是一个类型
		iterator begin() {
			return _mp.begin();
		}
		iterator end() {
			return _mp.end();
		}
		void Inorder() {
			//return _mp.Inorder();
			Node* root = _mp.GetRoot();
			_Inorder(root);
		}
		std::pair<iterator, bool> insert(const std::pair<K, V> kv) {
			return _mp.Insert(kv);
		}
		iterator find(const K& key) {
			return _mp.find(key);
		}
		V& operator[](const K& key) {
			auto ret = _mp.Insert(std::make_pair(key, V()));
			//const type_info& info = typeid(ret);
			//std::cout << info.name() << std::endl;
			return ret.first->second;
		}
	private:
		void _Inorder(Node* root) {
			if (root == nullptr)
				return;
			_Inorder(root->_left);
			std::cout << root->_data.first << " : " << root->_data.second << std::endl;
			_Inorder(root->_right);
		}
	private:
		RBTree<K, std::pair<K, V>, MapKeyOfT> _mp;
	};

	void test_map() {
		map<int, int> mp;
		mp.insert({ 1, 1 });
		mp.insert({ 6, 1 });
		mp.insert({ 4, 1 });
		mp.insert({ 9, 1 });
		mp.insert({ 2, 1 });
		mp[1]++;
		mp.Inorder();
		map<int, int> mp1(mp);
		map<int, int>::iterator it = mp1.begin();
		while (it != mp1.end()) {
			std::cout << it->first << " : " << it->second << std::endl;
			++it;
		}
		map<int, int> mp2;
		mp2.insert({ 0, 0 });
		mp2 = mp;
		mp2.Inorder();
	}
}