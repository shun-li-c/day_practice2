#pragma once
#include"RBTree.h"

namespace zhu {
	template<class K>
	class set {
	public:
		struct SetKeyOfT {
			const K& operator()(const K& k) {
				return k;
			}
		};

		typedef typename RBTree<K, K, SetKeyOfT>::iterator iterator;

		iterator begin() {
			return _st.begin();
		}

		iterator end() {
			return _st.end();
		}
		std::pair<iterator, bool> insert(const K& key) {
			return _st.Insert(key);
		}
		iterator find(const K& key) {
			return _st.find(key);
		}
		void InOrder() {
			_st.Inorder();
		}
	private:
		RBTree<K, K, SetKeyOfT> _st;
	};

	void test_set() {
		set<int> st;
		st.insert(5);
		st.insert(1);
		st.insert(8);
		st.insert(3);
		st.insert(4);
		st.InOrder();

		set<int> copy(st);
	}
}