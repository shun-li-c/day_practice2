#pragma once
#include<iostream>
#include<assert.h>
#include<algorithm>

namespace zhu {
	template<class T>
	struct ListNode {
		ListNode* _prev;
		ListNode* _next;
		T _data;
	
		ListNode(const T& data = T())
			:_prev(nullptr)
			,_next(nullptr)
			,_data(data)
		{}
	};

	template<class T, class Ref, class Ptr>
	class __iterator_list {
		typedef ListNode<T> Node;
		typedef __iterator_list<T, Ref, Ptr> self;
	public:
		Node* _node;
	public:
		__iterator_list(Node* listN) 
			:_node(listN)
		{}

		Ref operator*() {
			return _node->_data;
		}
		Ptr operator->() {
			return &(_node->_data);
		}

		self& operator++() {
			_node = _node->_next;
			return *this;
		}
		self operator++(int) {
			self temp(*this);
			_node = _node->_next;
			return temp;
		}
		self& operator--() {
			_node = _node->_prev;
			return *this;
		}
		self operator--(int) {
			self temp(*this);
			_node = _node->_prev;
			return temp;
		}
		bool operator!=(const self& it) const {
			return _node != it._node;
		}
		bool operator==(const self& it) const {
			return _node == it._node;
		}
	};
	//反向迭代器,复用正向迭代器实现
	// Iterator是哪个容器的迭代器，reverse_iterator<Iterator>就可以
	// 适配出哪个容器的反向迭代器。复用的体现
	template<class Iterator, class Ref, class Ptr>
	class reverse_iterator {
		Iterator _it;
		typedef reverse_iterator<Iterator, Ref, Ptr> self;
	public:
		reverse_iterator(Iterator it) 
			:_it(it)
		{}
		Ref operator*() {
			//返回的是迭代器前一个位置,根rbegin和rend位置有关
			Iterator prev = _it;
			return *--prev;
		}
		Ptr operator->() {
			return &operator*();
		}
		self& operator++() {
			--_it;
			return *this;
		}
		self& operator--() {
			++_it;
			return *this;
		}
		bool operator!=(const self& rit)const {
			return _it != rit._it;
		}
	};
	template<class T>
	class list {
	public:
		typedef ListNode<T> Node;
		typedef __iterator_list<T, T&, T*> iterator;
		typedef __iterator_list<T, const T&, const T*> const_iterator;

		typedef reverse_iterator<const_iterator, const T&, const T*> const_reverse_iterator;
		typedef reverse_iterator<iterator, T&, T*> reverse_iterator;
		
		list(const T& val = T()) 
			:_head(new Node)
		{
			_head->_next = _head;
			_head->_prev = _head;
			_head->_data = val;
		}
		list(const list<T>& lt) {
			_head = new Node;
			_head->_next = _head;
			_head->_prev = _head;
			for (auto& e : lt) {
				push_back(e);
			}
		}
		list<T>& operator=(const list<T> lt) {
			if (this != &lt) {
				clear();
				for (auto e : lt) {
					push_back(e);
				}
			}
			return *this;
		}
		//现代写法拷贝构造首先要实现出迭代器区间的构造函数
		//InputIterator表示任意类型的迭代器,只读型
		//template<class InputIterator>
		//list(InputIterator begin, InputIterator end) {
		//	_head = new Node;
		//	_head->_prev = _head;
		//	_head->_next = _head;
		//	while (begin != end) {
		//		push_back(*begin);
		//		++begin;
		//	}
		//}
		////lt2(lt1)
		//list(const list<T>& lt) {
		//	_head = new Node;
		//	_head->_next = _head;
		//	_head->_prev = _head;

		//	list<T> temp(lt.begin(), lt.end());
		//	std::swap(_head, temp._head);
		//}
		//list<T>& operator=(list<T> lt) {
		//	std::swap(_head, lt._head);
		//	return *this;
		//}
		~list() {
			clear();
			delete _head;
			_head = nullptr;
		}
		void push_back(const T& val){
			Node* NewNode = new Node(val);
			Node* tail = _head->_prev;
			NewNode->_prev = tail;
			NewNode->_next = _head;

			tail->_next = NewNode;
			_head->_prev = NewNode;

		}
		void push_front(const T& val) {
			Node* NewNode = new Node(val);
			Node* next = _head->_next;

			_head->_next = NewNode;
			next->_prev = NewNode;
			NewNode->_prev = _head;
			NewNode->_next = next;
		}
		void pop_back() {
			Node* OldTail = _head->_prev;
			Node* NewTail = OldTail->_prev;
			NewTail->_next = _head;
			_head->_prev = NewTail;
			delete OldTail;
		}
		void pop_front() {
			Node* Oldfront = _head->_next;
			_head->_next = Oldfront->_next;
			Oldfront->_next->_prev = _head;
			delete Oldfront;
		}
		void insert(iterator pos, const T& val) {
			Node* NewNode = new Node(val);
			Node* prev = (pos._node)->_prev;
			
			NewNode->_next = pos._node;
			NewNode->_prev = prev;
			prev->_next = NewNode;
			(pos._node)->_prev = NewNode;
		}
		iterator erase(iterator pos) {
			assert(pos != end());
			Node* prev = pos._node->_prev;
			Node* next = pos._node->_next;

			prev->_next = next;
			next->_prev = prev;
			delete pos._node;
			//迭代器失效进行更新
			return iterator(next);
		}
		void clear() {
			iterator it = begin();
			while (it != end()) {
				erase(it++);
			}
		}
		iterator begin() {
			return iterator(_head->_next);
		}
		iterator end() {
			return iterator(_head);
		}
		reverse_iterator rbegin() {
			return reverse_iterator(_head);
		}
		reverse_iterator rend() {
			return reverse_iterator(_head->_next);
		}
		const_iterator begin() const {
			return const_iterator(_head->_next);
		}
		const_iterator end() const {
			return const_iterator(_head);
		}

	private:
		Node* _head;
	};

	class Date {
	public:
		int _year;
		int _mouth;
		int _day;
	
		Date(int year = 0, int mouth = 0, int day = 0) 
			:_year(year)
			,_mouth(mouth)
			,_day(day)
		{}
	};
	void printf_list(const list<Date>& lt)
	{
		list<Date>::const_iterator it = lt.begin();
		while (it != lt.end()) {
			it._node->_data = Date(2000, 1, 1);
			std::cout << it->_year << " " << it->_mouth << " " << it->_day << std::endl;
			++it;
		}
	}
	//void printf_list(const list<int>& lt)
	//{
	//	list<int>::const_iterator it = lt.begin();
	//	while (it != lt.end()) {
	//		*it = 10;
	//		//std::cout << it->_year << " " << it->_mouth << " " << it->_day << std::endl;
	//		++it;
	//	}
	//}
	void test_list(){
		list<int> lt;
		lt.push_back(3);
		lt.push_back(4);
		lt.push_back(5);
		lt.push_front(6);

		list<int> lt2;
		lt2.push_back(7);
		lt2.push_back(8);
		lt2.push_back(9);

		lt = lt2;
		for (auto& l : lt) {
			std::cout << l << std::endl;
		}
		//list<Date> lt1;
		//lt1.push_back(Date(2023, 1, 13));
		//lt1.push_back(Date(2023, 1, 14));
		//lt1.push_back(Date(2023, 1, 15));
		//lt1.pop_back();
		//lt1.pop_front();
		//lt1.insert(lt1.begin(), Date(2023, 1, 16));
		//list<Date>::reverse_iterator it = lt1.rbegin();
		//while (it != lt1.rend()){
		//	std::cout << it->_year << " " << it->_mouth << " " << it->_day << std::endl;
		//	++it;
		//}
		////printf_list(lt1);
		//std::cout << std::endl;
	}
}