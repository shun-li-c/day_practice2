#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <unordered_map>
using namespace std;

int Strcmp(string str1, string str2) {

    int len = str1.size();
    for(int i = 0; i < len; i++){
        if (str1[i] > str2[i] || str1[i] < str2[i]) {
            return -1;
        }
    }
    return 0;
}

int test() {
    vector<pair<string, string>> sv;
    int n = 0;
    int first = 0, second = 0;
    cin >> n;
    while (n--) {
        cin >> first >> second;
        sv.push_back({ to_string(first), to_string(second) });
    }

    string ret;

    for (auto& pr : sv) {
        string tmp = pr.first;
        sort(tmp.begin(), tmp.end());
        if (tmp[0] == '0') {
            //若首字母为0则向后找第一个不为0的字符进行交换
            int i = 1;
            while (tmp[i] == '0') {
                i++;
            }
            swap(tmp[0], tmp[i]);
        }

        if (Strcmp(tmp.c_str(), pr.second.c_str()) == 0) {
            ret += '1';
        }
        else {
            ret += '0';
        }
    }
    cout << ret << endl;
    return 0;
}


class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param a int整型vector 输入数组
     * @return long长整型
     */
    long long CalcTotal(vector<int>& a) {
        int n = a.size();
        vector<int> arr(n, 0);

        for (int i = 1; i < n; i++) {

            int tmp = i - 1;
            while (tmp >= 0 && a[i] > a[tmp]) {
                arr[i] += (a[i] + a[tmp]);
                tmp--;
            }
        }

        long long ret = 0;
        for (int i = 0; i < n; i++) {
            ret += arr[i];
        }
        return ret;
    }
};

void test2() {
    //string str1 = "1033", str2 = "1033";
    //cout << Strcmp(str1.c_str(), str2.c_str());

    vector<int> v{ 1, 8, 2, 7, 9 };
    Solution slu;
    slu.CalcTotal(v);
}

void test_unordered_map() {
    unordered_map<int, int> ump{ {1, 1},{2, 2}, {3, 3}, {4, 4}, {6, 6} };
    auto it = ump.begin();
    while (it != ump.end()) {
        if (it->first % 2 == 0) {
            it = ump.erase(it);
            continue;
        }
        ++it;
    }
    for (auto val : ump) {
        cout << val.first << " ";
    }
}
int main() {

    test_unordered_map();
    return 0;
}
