#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<unordered_map>
using namespace std;

//两数之和
class Solution {
public:
    //暴力枚举
    // vector<int> twoSum(vector<int>& nums, int target) {
    //     vector<int> v;
    //     for(int i = 0; i < nums.size(); i++){
    //         for(int j = i + 1; j < nums.size(); j++){
    //             if(nums[i] + nums[j] == target){
    //                 v.push_back(i);
    //                 v.push_back(j);
    //                 break;
    //             }
    //         }
    //     }
    //     return v;
    // }

    //哈希表
    vector<int> twoSum(vector<int>& nums, int target) {
        unordered_map<int, int> hashtable;
        for (int i = 0; i < nums.size(); i++) {
            auto it = hashtable.find(target - nums[i]);
            if (it != hashtable.end()) {
                return { it->second, i };
            }
            hashtable[nums[i]] = i;
        }
        return {};
    }
};


int main() {

    return 0;
}