#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
using namespace std;


//------------------二叉树序列化与反序列化------------------/
struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
    TreeNode(int x) :
            val(x), left(NULL), right(NULL) {
    }
};

#include <string>
class Solution {
public:
    void inorder(TreeNode* root, string& str) {
        if (root == nullptr) {
            str += "#";
            return;
        }
        str += to_string(root->val) + ",";
        inorder(root->left, str);
        inorder(root->right, str);
    }
    char* Serialize(TreeNode* root) {
        string str;
        inorder(root, str);
        char* ret = new char[str.size() - 1];
        strcpy(ret, str.c_str());
        return ret;
    }
    TreeNode* _Deserialize(char* str, int& index) {
        if (str[index] == '#') {
            ++index;
            return nullptr;
        }
        bool sign = false; //标志节点值为正负
        if (str[index] == '-') {
            sign = true;
            ++index;
        }
        int val = 0;
        while (str[index] != ',') {
            val = val * 10 + (str[index] - '0');
            ++index;
        }
        if (sign) {
            val = -val;
        }
        ++index;
        TreeNode* root = new TreeNode(val);
        root->left = _Deserialize(str, index);
        root->right = _Deserialize(str, index);
        return root;
    }
    TreeNode* Deserialize(char* str) {
        int index = 0;
        return _Deserialize(str, index);
    }
};


int main() {

	return 0;
}

