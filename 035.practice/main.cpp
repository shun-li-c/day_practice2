#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<queue>
using namespace std;

class A {
public:
	int a;
	int b;
	A() {
		a = 10;
		b = 20;
	}
};

class B : private A {
public:
	void printf() {
		cout << a << " " << b << endl;
	}
};

//--------------------------------常见排序算法-----------------------------------/
//插入排序
//前n - 1个数据想象成有序,拿最后一个进行插入操作
//时间复杂度O(N^2)
//稳定排序
void InsertSort(vector<int>& v) {
	int n = v.size();
	for (int i = 0; i < n - 1; i++) {
		int end = i;
		//temp为待插入值
		int temp = v[end + 1];
		while (end >= 0)
		{
			if (v[end] > temp) {
				//交换
				v[end + 1] = v[end];
				end--;
			}
			else {
				break;
			}
		}
		v[end + 1] = temp;
	}
}
void test_InsertSort() {
	vector<int> v{4, 1, 6, 8, 3, 32, 88, 12};
	InsertSort(v);
	for (auto& val : v) {
		cout << val << " ";
	}
	cout << endl;
}
//-----------------------------希尔排序(升级)--------------------------/
//不稳定
//时间复杂度O(N^1.3 ~ N^2)
void ShellSort(vector<int>& v) {
	int n = v.size();
	int gap = n;
	while (gap > 1) {
		gap = (gap / 3) + 1;
		for (int i = 0; i < n - gap; i++) {
			int end = i;
			//temp为待插入值
			int temp = v[end + gap];
			while (end >= 0)
			{
				if (v[end] > temp) {
					//交换
					v[end + gap] = v[end];
					end -= gap;
				}
				else {
					break;
				}
			}
			v[end + gap] = temp;
		}
	}
}
void test_ShellSort() {
	vector<int> v{ 4, 1, 6, 8, 3, 32, 88, 12 };
	ShellSort(v);
	for (auto& val : v) {
		cout << val << " ";
	}
	cout << endl;
}
//----------------------------选择排序-------------------------------/
//基本思想每轮次从数组中选出最大值和最小值,分别放在队尾和队首
//时间复杂度O(N^2)
//不稳定
void SelectSort(vector<int>& v) {
	int left = 0, right = v.size() - 1;
	while (left < right)
	{
		int MaxSub = left, MinSub = left;
		for (int i = left; i <= right; i++) {
			if (v[MaxSub] < v[i])
				MaxSub = i;
			if (v[MinSub] > v[i])
				MinSub = i;
		}
		
		//进行交换,注意不能简单进行交换会有覆盖问题
		swap(v[MinSub], v[left]);
		//left可能为最大值位置
		if (left == MaxSub)
			MaxSub = MinSub;
		swap(v[MaxSub], v[right]);
		++left;
		--right;
	}
}
void test_SelectSort() {
	vector<int> v{ 4, 1, 6, 8, 3, 32, 88, 12 };
	SelectSort(v);
	for (auto& val : v) {
		cout << val << " ";
	}
	cout << endl;
}


//---------------------------交换排序----------------------/
//冒泡排序
//思想一次遍历排好一个位置
//时间复杂度O(N^2)
//稳定
void BubbleSort(vector<int>& v) {
	int begin = 0, end = v.size() - 1;
	for (int i = begin; i < end; i++) {
		for (int j = 0; j < end - i; j++) {
			if (v[j] > v[j + 1]) {
				swap(v[j], v[j + 1]);
			}
		}
	}
}
void test_BubbleSort() {
	vector<int> v{ 4, 1, 6, 8, 3, 32, 88, 12 };
	BubbleSort(v);
	for (auto& val : v) {
		cout << val << " ";
	}
	cout << endl;
}
//归并排序
//思想:区间划分,先合并小区间后合并大区间
//时间复杂度O(N*log(N))
//空间复杂度O(N)
//稳定
void MergeSort(vector<int>& v, vector<int>& temp, int left, int right) {
	if (left >= right)
		return;
	int mid = (left + right) >> 1;
	int begin1 = left, end1 = mid;
	int begin2 = mid + 1, end2 = right;

	MergeSort(v, temp, begin1, end1);
	MergeSort(v, temp, begin2, end2);

	int index = left;
	while (begin1 <= end1 && begin2 <= end2) {
		//选小的插到临时数组中
		if (v[begin1] < v[begin2])
			temp[index++] = v[begin1++];
		else {
			temp[index++] = v[begin2++];
		}
	}

	while (begin1 <= end1) {
		temp[index++] = v[begin1++];
	}
	while (begin2 <= end2) {
		temp[index++] = v[begin2++];
	}

	for (int i = left; i <= right; i++) {
		v[i] = temp[i];
	}
}

void test_MergeSort() {
	vector<int> v{ 4, 1, 6, 8, 3, 32, 88, 12 };
	vector<int> temp(v.size(), 0);
	MergeSort(v, temp, 0, v.size() -1);
	for (auto& val : v) {
		cout << val << " ";
	}
	cout << endl;
}

//---------------------------计数排序----------------------------/
//思想:哈希直接定值法应用
//考虑开辟空间,最大值减最小值
//时间复杂度：O(max(n，开辟空间))
//稳定
void CountSort(vector<int>& v) {
	int n = v.size();
	//找出最大值和最小值
	int max = v[0], min = v[0];
	for (int i = 0; i < n; i++) {
		if (max < v[i])
			max = v[i];
		if (min > v[i])
			min = v[i];
	}
	//开辟一段空间进行映射
	vector<int> tmp((max - min + 1), 0);
	for (int i = 0; i < n; i++) {
		tmp[v[i] - min]++;
	}

	int index = 0;
	//根据映射还原回原数组中去
	for (int i = 0; i < max - min + 1; i++) {
		while (tmp[i] > 0) {
			v[index++] = i + min;
			tmp[i]--;
		}
	}
}

void test_CountSort() {
	vector<int> v{ 4, 1, 6, 8, 3, 32, 88, 12 };
	vector<int> temp(v.size(), 0);
	CountSort(v);
	for (auto& val : v) {
		cout << val << " ";
	}
	cout << endl;
}

//-------------------------------快排--------------------------------/
//霍尔法
//先选定最左或最右边值为参考,左边找大的值右边找小的值进行交换
//最后将key放在相遇位置
//时间复杂度:O(n	log(n))
int _HoreSort(vector<int>& v, int left, int right) {
	
	int key = left;
	while (left < right) {
		//右边找小的值
		while (left < right && v[right] >= v[key]) {
			right--;
		}
		//左边找大的值
		while (left < right && v[left] <= v[key]) {
			left++;
		}
		swap(v[left], v[right]);
	}
	swap(v[left], v[key]);
	return left;
}
void HoreSort(vector<int>& v, int left, int right) {
	if (left > right)
		return;

	int meet = _HoreSort(v, left, right);
	HoreSort(v, left, meet - 1);
	HoreSort(v, meet + 1, right);
}
void test_HoreSort() {
	vector<int> v{ 4, 1, 6, 8, 3, 32, 88, 12 };
	vector<int> temp(v.size(), 0);
	HoreSort(v, 0, v.size() - 1);
	for (auto& val : v) {
		cout << val << " ";
	}
	cout << endl;
}
//-------------------------------堆排序------------------------------/
//排升序建大堆，拍降序建小堆
//建堆与堆排序向下调整算法,堆的插入向上调整算法
//时间复杂度：O(n	log(n))
//堆排不稳定
void AdjustUp(vector<int>& v) {
	int last_child = v.size() - 1;
	int parent = (last_child - 1) / 2;
	while (last_child != 0) {
		if (v[last_child] > v[parent]) {
			swap(v[last_child], v[parent]);
			last_child = parent;
			parent = (last_child - 1) / 2;
		}
		else {
			break;
		}
	}
}
//首先建大堆(向下调整算法)
void AdjustDown(vector<int>& v, int n, int parent) {
	//根据父子位置下标来推出最后一个父节点下标
	int child = parent * 2 + 1;
	while (child < n) {
		//选出左右孩子中较大的那个和父节点进行比较
		if (child + 1 < n && v[child + 1] > v[child]) {
			child++;
		}

		if (v[child] > v[parent]) {
			swap(v[child], v[parent]);
			parent = child;   //交换会打乱原有的结构,继续向下调整
			child = parent * 2 + 1;
		}
		else {
			break;
		}
	}
}
void HeapSort(vector<int>& v) {
	int n = v.size();
	int parent = (n - 1) / 2;
	for (int i = parent; i >= 0; i--) {
		AdjustDown(v, n, i);
	}
	int end = v.size() - 1;
	while (end) {
		swap(v[0], v[end]);
		AdjustDown(v, end, 0);
		end--;
	}
	//v.push_back(20);
	//AdjustUp(v);
}
void test_HeapSort() {
	vector<int> v{ 4, 1, 6, 8, 3, 32, 88, 12 };
	vector<int> temp(v.size(), 0);
	HeapSort(v);
	for (auto& val : v) {
		cout << val << " ";
	}
	cout << endl;
}
void test_priority_queue() {
	priority_queue<int, vector<int>, greater<int>> hp;
	hp.push(7);
	hp.push(45);
	hp.push(33);
	hp.push(99);
	hp.push(20);
	hp.push(4);
	while (!hp.empty()) {
		cout << hp.top() << " ";
		hp.pop();
	}
}
int main() {
	
	//B b;
	//b.printf();
	//test_InsertSort();
	//test_ShellSort();
	//test_SelectSort();
	//test_BubbleSort();
	//test_MergeSort();
	//test_CountSort();
	//test_HoreSort();
	//test_HeapSort();
	test_priority_queue();
	return 0;
}
