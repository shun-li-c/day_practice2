#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<string>
using namespace std;

//������λ�ַ�����¼��λ��
class Solution {
public:
    vector<int> findAnagrams(string s, string p) {
        int s_size = s.size(), p_size = p.size();
        if (s_size < p_size)
            return {};
        vector<int> hashTable(26, 0);
        for (int i = 0; i < p_size; i++) {
            ++hashTable[p[i] - 'a'];
        }
        vector<int> ret;
        for (int l = 0, r = 0; r < s_size; r++) {
            --hashTable[s[r] - 'a'];
            while (hashTable[s[r] - 'a'] < 0) {
                //�ƶ��󴰿�,�ָ�ӳ��ֵ
                ++hashTable[s[l] - 'a'];
                ++l;
            }

            if (r - l + 1 == p_size)
                ret.push_back(l);
        }
        return ret;
    }

};
int main() {
	return 0;
}

