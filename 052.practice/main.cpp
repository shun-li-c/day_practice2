#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

void bubbleSort(int arr[], int size) {
	int flag = 0;
	for (int i = 0; i < size - 1; i++) {
		for (int j = 0; j < size - 1 - i; j++) {
			if (arr[j] > arr[j + 1]) {
				int tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
				flag = 1;
			}
			if (flag == 0)
				break;
		}
	}
}
int main() {
	//int arr[] = { 3, 1, 6, 6, 99, 30, 4, 0 };
	int arr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	bubbleSort(arr, 8);
	for (auto val : arr) {
		cout << val << " ";
	}
	return 0;
}

