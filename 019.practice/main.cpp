#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

class Solution {
public:
    bool match(string str, string pattern) {
        int n1 = str.length();
        int n2 = pattern.length();
        //dp[i][j]表示前i个字符和pattern前j个字符匹配
        vector<vector<bool>> dp(n1 + 1, vector<bool>(n2 + 1, false));
        dp[0][0] = true;
        //初始化字符串为空的情况,字符串下标从1开始
        for (int i = 1; i <= n2; i++) {
            if (pattern[i - 1] == '*') {
                //与前一个能够匹配字符串有关
                dp[0][i] = dp[0][i - 2];
            }
        }
        //遍历str每个字符串的长度
        for (int i = 1; i <= n1; i++) {
            //遍历每个parent字符串的长度
            for (int j = 1; j <= n2; j++) {
                if (pattern[j - 1] != '*' && (pattern[j - 1] == '.' || pattern[j - 1] == str[i - 1])) {
                    dp[i][j] = dp[i - 1][j - 1];
                }
                else if (j >= 2 && pattern[j - 1] == '*') {
                    if (pattern[j - 2] == str[i - 1] || pattern[j - 2] == '.') {
                        dp[i][j] = dp[i - 1][j] || dp[i][j - 2];
                    }
                    else {
                        dp[i][j] = dp[i][j - 2];
                    }
                }
            }
        }
        return dp[n1][n2];
    }
};

int main() {
    //Solution sl;
    //sl.match("aaa", "ab*ac*a");
    //cout << sizeof(double) << endl;
    long long a = 1, b = 2, c = 3;
    printf("%d %d %d\n", c, b, a);
    return 0;
}

