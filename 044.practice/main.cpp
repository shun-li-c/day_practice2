#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
#include<vector>
#include<sstream>
using namespace std;
typedef unsigned int UINT;
typedef unsigned char UCHAR;

//小端转大端
UINT EndianConvertLToB(UINT InputNum) {
	UCHAR* p = (UCHAR*)&InputNum;
	return(((UINT)*p << 24) + ((UINT) * (p + 1) << 16) +
		((UINT) * (p + 2) << 8) + (UINT) * (p + 3));
}

//大端转小端
UINT EndianConvertBToL(UINT InputNum) {
	UCHAR* p = (UCHAR*)&InputNum;
	return(((UINT)*p) + ((UINT) * (p + 1) << 8) +
		((UINT) * (p + 2) << 16) + (UINT) * (p + 3) << 24);
}

int test() {
	int a = 1;
	if (*(char*)&a == 1)
		cout << "xiaoduan" << endl;

	UINT i = 0x123456;
	//cout << EndianConvertLToB(i) << endl;
	UCHAR* p = (UCHAR*)&i;
	std::ostringstream str;
	str << std::hex << (UINT)*p << (UINT) * (p + 1) << (UINT) * (p + 2) << (UINT) * (p + 3);
	cout << str.str() << endl;

	return 0;
}

class Solution {
public:
    static int maxProfit(vector<int>& prices) {
        int n = prices.size();
        if (n <= 1)
            return 0;
        vector<vector<int>> dp(n, vector<int>(3, 0));

        //初始化
        dp[0][0] = 0;
        dp[0][1] = -1 * prices[0];
        dp[0][2] = 0;

        for (int i = 1; i < n; i++) {
            dp[i][0] = max(dp[i - 1][0], dp[i - 1][2]);
            dp[i][1] = max(dp[i - 1][1], dp[i - 1][0] - prices[i]);
            dp[i][2] = dp[i - 1][1] + prices[i];
        }

		//for (int i = 0; i < n; i++) {
		//	cout << dp[i][0] << " " << dp[i][1] << " " << dp[i][2] << endl;
		//}
        return max(dp[n - 1][0], dp[n - 1][2]);
    }
};

int fibi(int n) {
	if (n == 1)
		return 1;
	if (n == 0)
		return 0;
	
	return fibi(n - 1) + fibi(n - 2);
}
int test1() {
	vector<int> arr{ 1, 2, 3, 0, 2 };
	Solution::maxProfit(arr);
	int n = 6;
	cout << fibi(n) << endl;
	return 0;
}
void NumberOf1(int n) {
	int count = 0;

	while (n) {
		n = (n - 1) & n;
		count++;
	}
	cout << count << endl;
}
void test2() {
	NumberOf1(15);
	const char* str = "asdfddd";
	cout << sizeof(str) << endl;
	char arr[] = "asdfg";
	cout << sizeof(arr) << endl;
	cout << strlen(arr) << endl;
}
int main() {
	test2();
	return 0;
}