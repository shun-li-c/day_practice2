#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<assert.h>
using namespace std;

namespace zhu {
	void* memmove(void* dest, void* src, size_t count) {
		assert(dest);
		void* ret = dest;
		//解决了内存重叠问题
		if (dest < src) {
			//从前向后拷贝
			while (count--) {
				*(char*)dest = *(char*)src;
				dest = (char*)dest + 1;
				src = (char*)src + 1;
			}
		}
		else {
			//从后向前拷贝
			while (count--) {
				*((char*)dest + count) = *((char*)src + count);
			}
		}
		return ret;
	}
	void* memcpy(void* dest, const void* src, size_t count) {
		assert(dest);
		void* ret = dest;
		while (count--) {
			*(char*)dest = *(char*)src;
			dest = (char*)dest + 1;
			src = (char*)src + 1;
		}
		return ret;
	}
	char* strstr(const char* dst, const char* src) {
		assert(dst && src);
		char* t_str1 = (char*)dst;
		char* t_str2 = (char*)src;
		char* ret = nullptr;
		while (*t_str1 != '\0') {
			ret = t_str1;
			t_str2 = (char*)src;
			while (*t_str2 && *t_str1 == *t_str2) {
				t_str1++;
				t_str2++;
			}
			t_str1 = ret;
			if (*t_str2 == '\0')
				return ret;
			++t_str1;
		}
		return nullptr;
	}
}
int main() {
	char str[] = "aasd";
	char str1[] = "asd";
	//zhu::memmove(str, str1, 4);
	//cout << str << endl;
	int arr[] = { 4, 7, 1, 3, 9, 8, 10 };
	//zhu::memmove(arr, arr + 2, 3 * sizeof(int));
	//zhu::memmove(arr + 2, arr, 4 * sizeof(int));
	//zhu::memcpy(arr, arr + 2, 4 * sizeof(int));
	//for (int& e : arr) {
	//	cout << e << " ";
	//}
	//cout << endl;
	char* ret = zhu::strstr(str, str1);
	if (ret == nullptr) {
		cout << "找不到" << endl;
	}
	else {
		cout << "找到了" << endl;
		cout << ret << endl;
	}
	return 0;
}
