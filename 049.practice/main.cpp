#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;
class Cmp {
public:
    bool operator()(vector<int> a, vector<int> b) {
        if (a[0] > b[0]) {
            return true;
        }
        else if (a[0] == b[0]) {
            if (a[1] < b[1])
                return true;
            else
                return false;
        }
        else {
            return false;
        }
    }
};
class Solution {
public:
    vector<vector<int>> reconstructQueue(vector<vector<int>>& people) {
        //排好序后按照第二个参数进行插入排序即可
        sort(people.begin(), people.end(), Cmp());


        //顺序遍历放到对应位置即可
        for (int i = 1; i < people.size(); i++) {
            vector<int> tmp = people[i];
            int index = i;
            int plance = tmp[1];
            while (index > plance) {
                swap(people[index], people[index - 1]);
                index--;
            }
        }
        return people;
    }
};

int main() {
    return 0;
}
