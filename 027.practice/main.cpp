#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<map>
#include<string>
#include<queue>
using namespace std;
int main() {
	priority_queue<int, vector<int>, greater<int>> heap;
	heap.push(1);
	heap.push(2);
	heap.push(3);
	heap.push(4);
	while (!heap.empty()) {
		cout << heap.top() << " ";
		heap.pop();
	}
	return 0;
}
