#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

void bubbleSort(int *p, int len) {
	int flag = 0;
	for (int i = 0; i < len - 1; i++) {
		for (int j = 0; j < len - 1 - i; j++) {
			if (p[j] > p[j + 1]) {
				int tmp = p[j + 1];
				p[j + 1] = p[j];
				p[j] = tmp;
				flag = 1;
			}
		}
		if (flag == 0)
			break;
	}
	int a = 10;
}
class A {
public:
	virtual void printf() {
		cout << 'A' << endl;
	}
};
class B : A {
public:
	virtual void printf() {
		cout << "B:A" << endl;
	}
};
int test() {
	//int arr[] = { 3, 5, 1, 6, 2 };
	//bubbleSort(arr, sizeof(arr) / sizeof(arr[0]));
	//for (auto& v : arr) {
	//	cout << v << " ";
	//}

	B b1;
	B b2;
	B b3;
	return 0;
}

struct ListNode {
	int val;
	ListNode* next;
	ListNode() : val(0), next(nullptr) {}
	ListNode(int x) : val(x), next(nullptr) {}
	ListNode(int x, ListNode* next) : val(x), next(next) {}
	
};

class Solution {
public:
	static bool isPalindrome(ListNode* head) {
		if (head == nullptr || head->next == nullptr)
			return true;
		ListNode* prev = nullptr, * cur = head, * next = head->next;
		//进行反转
		while (next) {
			cur->next = prev;
			prev = cur;
			cur = next;
			next = next->next;
		}
		cur->next = prev;
		//此时cur为反转后的头
		while (cur && head) {
			if (cur->val != head->val)
				return false;
			cur = cur->next;
			head = head->next;
		}
		return true;
	}
};
void test1() {
	ListNode l1(1);
	ListNode l2(1);
	ListNode l3(2);
	ListNode l4(1);
	l1.next = &l2;
	l2.next = &l3;
	l3.next = &l4;
	Solution::isPalindrome(&l1);
}
int main() {
	int from, to;
	while (cin >> from >> to) {
		//计算第n个斐波那契额数
		int n1 = 0, n2 = 1, retNum = 1, num = 0;
		for (int i = 2; i <= to; i++) {
			num = n1 + n2;
			n1 = n2;
			n2 = num;
			if (i >= from)
				retNum += num;
		}

		cout << (from > 1 ? retNum - 1 : retNum) << endl;
	}
}
//int main() {
//	test1();
//	return 0;
//}

