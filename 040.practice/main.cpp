#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
#include<iostream>
#include<list>
#include<algorithm>
#include<unordered_map>
using namespace std;

class LRUCache {
private:
    int _cap;
    list<pair<int, int>> _cache;
    unordered_map<int, list<pair<int, int>>::iterator> _ump;
public:
    LRUCache(int capacity) : _cap(capacity) {}

    int get(int key) {
        return 0;
    }

    void put(int key, int value) {
        //插入有两种情况,一是已经存在要删除重新插入
        //而是不存在进行新增(要考虑扩容问题)
        if (_ump.find(key) == _ump.end()) {
            //不存在进行新增
            if (_cap == _cache.size()) {
                //容量满了,删除链表尾部
                _ump.erase(_cache.back().first);
                _cache.pop_back();
            }
        }
        else {
            //存在删除
            _cache.erase(_ump[key]);
        }
        //正常插入
        _cache.push_front({ key, value });
        _ump[key] = _cache.begin();
    }
};


 struct ListNode {
     int val;
     ListNode *next;
     ListNode() : val(0), next(nullptr) {}
     ListNode(int x) : val(x), next(nullptr) {}
     ListNode(int x, ListNode *next) : val(x), next(next) {}
 };
 
class Solution {
public:
    ListNode* insertionSortList(ListNode* head) {
        if (head == nullptr)
            return nullptr;
        ListNode* THead = new ListNode(0);
        THead->next = head;
        ListNode* cur = head->next;
        ListNode* tail = head;

        while (cur) {
            ListNode* cur_next = cur->next;
            if (cur->val >= tail->val) {
                //直接进行尾插
                tail->next = cur;
                tail = tail->next;
            }
            else {
                ListNode* prev = THead;
                while (prev->next->val <= cur->val) {
                    prev = prev->next;
                }
                cur->next = prev->next;
                prev->next = cur;
            }
            cur = cur_next;
        }
        tail->next = nullptr;
        return THead->next;
    }
};

class cmp {
public:
    bool operator()(ListNode* node1, ListNode* node2) {
        return node1->val < node2->val;
    }
};
int main() {
    ListNode* l1 = new ListNode(4);
    ListNode* l2 = new ListNode(2);
    ListNode* l3 = new ListNode(1);
    ListNode* l4 = new ListNode(3);
    l1->next = l2;
    l2->next = l3;
    l3->next = l4;
    l4->next = nullptr;


    vector<ListNode*> vl;
    ListNode* tmp = l1;
    while (tmp) {
        vl.push_back(tmp);
        tmp = tmp->next;
    }
    sort(vl.begin(), vl.end(), cmp());
    for (int i = 1; i < vl.size(); i++) {
        vl[i - 1]->next = vl[i];
    }
    vl[vl.size() - 1]->next = nullptr;

    ListNode* ret = vl[0];
    while (ret) {
        cout << ret->val << " ";
        ret = ret->next;
    }
	return 0;
}
