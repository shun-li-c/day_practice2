#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
#include<vector>
using namespace std;

int* Add(int a, int b) {
	int sum = (a + b);
	return &sum;
}


class Solution {
public:
    static int coinChange(vector<int>& coins, int amount) {

        //定义：dp[i]为金额为i的最小数量
        vector<int> dp(amount + 1, amount + 1);
        for (auto val : dp)
            cout << val << " ";
        cout << endl;
        dp[0] = 0;

        for (int i = 0; i < dp.size(); i++) {
            for (int val : coins) {
                if (i - val < 0)
                    continue;
                dp[i] = min(dp[i], dp[i - val] + 1);
            }
        }

        for (auto val : dp)
            cout << val << " ";
        return dp[amount] == amount + 1 ? -1 : dp[amount];
    }
};
void test() {
    auto ret = Add(2, 3);
    *ret = 10;
    //cout << *ret << endl;
    vector<int> v{ 1, 2, 5 };
    Solution::coinChange(v, 11);
}
int main() {
	test();
	return 0;
}
