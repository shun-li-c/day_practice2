#pragma once

namespace zhu{
	template<class K>
	struct BSTNode {
		BSTNode<K>* _left;
		BSTNode<K>* _right;
		K _key;
		BSTNode(const K& key)
			:_left(nullptr)
			,_right(nullptr)
			,_key(key)
		{}
	};
	template<class K>
	class BSTree {
		typedef BSTNode<K> node;
	private:
		node* _root;
	public:
		BSTree()
			:_root(nullptr)
		{}
		//插入
		bool insert(const K& key) {
			node* parent = nullptr;
			node* cur = _root;
			//如果树为空直接插入
			if (_root == nullptr) {
				_root = new node(key);
				return true;
			}
			//不为空找到合适位置进行插入
			//搜索树特点-->左子树比根节点小,右子树比根节点大
			while (cur) {
				if (cur->_key > key) {
					parent = cur;
					cur = cur->_left;
				}
				else if (cur->_key < key) {
					parent = cur;
					cur = cur->_right;
				}
				else {
					return false;
				}
			}
			cur = new node(key);
			if (parent->_key > key) {
				parent->_left = cur;
			}
			else if (parent->_key < key) {
				parent->_right = cur;
			}
			else {
				return false;
			}
			return true;
		}
		//查找
		bool find(const K& key) {
			node* cur = _root;
			while (cur) {
				if (cur->_key > key) {
					cur = cur->_left;
				}
				else if (cur->_key < key) {
					cur = cur->_right;
				}
				else {
					return true;
				}
			}
			return false;
		}
		//删除
		bool erase(const K& key) {
			//先进行查找
			node* parent = nullptr;
			node* cur = _root;
			while (cur) {
				if (cur->_key > key) {
					parent = cur;
					cur = cur->_left;
				}
				else if (cur->_key < key) {
					parent = cur;
					cur = cur->_right;
				}
				else { //找到了,进行删除
					
					//左边为空
					if (cur->_left == nullptr) {
						
						//若删除的是根节点
						if (parent == nullptr) {
							_root = cur->_right;
						}
						else { //判断当前节点在parent节点的那个位置在进行连接
							if (cur == parent->_left)
								parent->_left = cur->_right;
							else if (cur == parent->_right)
								parent->_right = cur->_right;
						}
						delete cur;
					}
					else if (cur->_right == nullptr) {//右边为空
						//若删除的是根节点
						if (parent == nullptr) {
							_root = cur->_left;
						}
						else {//判断当前节点在parent节点的那个位置在进行连接
							if (cur == parent->_left)
								parent->_left = cur->_left;
							else if (cur == parent->_right)
								parent->_right = cur->_left;
						}
						delete cur;
					}
					else {//左右都不为空
						//找一个最小值来进行替换原来的值,删除最小值那个节点
						node* minParent = cur;
						node* min = cur->_right;
						while (min->_left) {
							minParent = min;
							min = min->_left;
						}
						cur->_key = min->_key; 
						if (minParent->_left == min) {
							minParent->_left = min->_right;
						}
						else {
							minParent->_right = min->_right;
						}
						delete min;
					}
					return true;
				}
			}
			return false;
		}
		void InOrder() {
			_InOrder(_root);
			std::cout << std::endl;
		}
		void _InOrder(node*& root) {
			if (root == nullptr) {
				return;
			}
			_InOrder(root->_left);
			std::cout << root->_key << " ";
			_InOrder(root->_right);
		}
	};
	
}

















