#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
#include"BinarySearchTree.h"

int main()
{
	zhu::BSTree<int> bst;
	bst.insert(5);
	bst.insert(3);
	bst.insert(7);
	bst.insert(4);
	bst.insert(1);
	bst.insert(2);
	bst.insert(0);
	bst.insert(9);
	bst.insert(6);
	bst.insert(8);
	bst.InOrder();
	bst.erase(0);
	bst.erase(7);
	bst.erase(5);
	bst.InOrder();
	return 0;
}

