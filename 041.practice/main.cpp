#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

int rob(vector<int>& nums) {
    if (nums.empty())
        return 0;
    int n = nums.size();
    int prev1 = nums[1], prev2 = nums[0];
    int ret = 0;
    for (int i = 2; i < n; i++) {
        //以当前位置偷或不偷来判断大小
        ret = max(prev1, prev2 + nums[i]);
        prev2 = prev1;
        prev1 = ret;
    }
    return ret;
}

void Printf() {
    int len = 0;
    cin >> len;
    for (int i = 0; i < len; i++) {
        for (int j = 0; j < len; j++) {
            if (i == 0 || i == len - 1 || j == 0 || j == len - 1) {
                cout << "* ";
            }
            else
                cout << "  ";
        }
        cout << endl;
    }
}
int main() {
    vector<int> v{ 1, 2, 3, 1 };
    //cout << rob(v);
    Printf();
    return 0;
}
