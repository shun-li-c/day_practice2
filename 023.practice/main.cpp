#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<unordered_set>
using namespace std;

class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        if (s.empty())
            return 0;
        //��������
        int right = 0;
        int max_len = 0;
        unordered_set<char> ust;

        for (int left = 0; left < s.length(); left++) {
            if (left != 0) {
                ust.erase(s[left - 1]);
            }

            while (right < s.length() && !ust.count(s[right])) {
                ust.insert(s[right]);
                right++;
            }

            max_len = max(max_len, right - left);
        }
        return max_len;
    }
};

int main() {
	return 0;
}