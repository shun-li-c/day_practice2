#pragma once
#include<vector>
namespace zhu {
	template<class T>
	class Less {
	public:
		bool operator()(const T& num1, const T& num2) {
			return num1 < num2;
		}
	};
	template<class T>
	class Greater {
	public:
		bool operator()(const T& num1, const T& num2) {
			return num1 > num2;
		}
	};
	//模板特化
	template<>
	class Greater<int> {
	public:
		bool operator()(const int& num1, const int& num2) {
			return num1 > num2;
		}
	};
	template<class T, class Container = std::vector<T>, class Compare = Greater<T>>
	class priority_queue {
	private:
		Container _con;
	public:
		priority_queue() {}
		template<class InputIterator>
		priority_queue(InputIterator begin, InputIterator end) 
			:_con(begin, end) //数据拿过来
		{
			//建堆
			for (int i = (_con.size() - 1 - 1) >> 2; i >= 0; i--) {
				adjust_down(i);
			}
		}
		void push(const T& val = 0) {
			_con.push_back(val);
			adjust_up(_con.size() - 1);
		}
		void pop() {
			std::swap(_con[0], _con[_con.size() - 1]);
			_con.pop_back();
			adjust_down(0);
		}
		size_t size() {
			return _con.size();
		}
		bool empty() {
			return _con.enpty();
		}
		const T& top() {
			return _con[0];
		}
	private:
		//向下调整算法
		void adjust_down(size_t parent) {
			int child = parent * 2 + 1;
			int n = _con.size();
			Compare Com;
			while (child < n) {
				if (child + 1 < n && Com(_con[child], _con[child + 1])) {
					child++;
				}
				if (Com(_con[parent], _con[child])) {//比较顺序不能颠倒
					std::swap(_con[child], _con[parent]);
				}
				parent = child;
				child = parent * 2 + 1;
			}
		}
		//向上调整算法
		void adjust_up(size_t child) {
			Compare Com;
			size_t parent = (child - 1) >> 1;
			while (child > 0) {
				if (Com(_con[parent], _con[child])) {
					std::swap(_con[child], _con[parent]);
					child = parent;
					parent = (child - 1) >> 1;
				}
				else {
					break;
				}
			}
		}
	};
	void test_PQueue() {
		std::vector<int> v{ 18, 19, 30, 12, 8 };
		//zhu::priority_queue<int> pq(v.begin(), v.end());
		zhu::priority_queue<int> pq;

		pq.push(18);
		pq.push(19);
		pq.push(30);
		pq.push(12);
		pq.push(8);

		int n = pq.size();
		for (int i = 0; i < n; i++) {
			std::cout << pq.top() << std::endl;
			pq.pop();
		}
	}
}







