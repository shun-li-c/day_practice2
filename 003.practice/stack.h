#pragma once
#include<queue>
namespace zhu {
	template<class T, class Container = std::deque<T>>
	class stack {
	private:
		Container _con;
	public:
		void push(const T& val) {
			_con.push_back(val);
		}
		void pop() {
			_con.pop_back();
		}
		const T& top() {
			return _con.back();
		}
		size_t size() {
			return _con.size();
		}
		bool empty() {
			return _con.empty();
		}
	};
	void test_stack() {
		stack<int> st;
		st.push(4);
		st.push(5);
		st.push(6);
		st.push(7);
		int size = st.size();
		for (int i = 0; i < size; i++) {
			std::cout << st.top() << std::endl;
			st.pop();
		}
	}
}