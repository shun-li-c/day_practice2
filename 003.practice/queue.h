#pragma once
#include<queue>
#include<iostream>
namespace zhu {
	template<class T, class Container = std::deque<T> >
	class queue {
	private:
		Container _con;
	public:
		void push(const T& val) {
			_con.push_back(val);
		}
		size_t size() {
			return _con.size();
		}
		bool empty() {
			return _con.empty();
		}
		const T& front() {
			return _con.front();
		}
		const T& back() {
			return _con.back();
		}
		void pop() {
			_con.pop_front();
		}
	};
	void test_queue() {
		queue<int> qe;
		qe.push(4);
		qe.push(5);
		qe.push(6);
		qe.push(7);
		int size = qe.size();
		for (int i = 0; i < size; i++) {
			std::cout << qe.front() << std::endl;
			qe.pop();
		}
		std::cout << "empty" << " " << qe.empty() << std::endl;
	}
}