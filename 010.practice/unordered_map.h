#pragma once
#include"LinkHash.h"
#include<iostream>
namespace zhu {
	template<class K, class V>
	class uonordered_map {
		struct mapKeyOFt {
			const K& operator()(const std::pair<K, V>& kv) {
				return kv.first;
			}
		};
	public:
		typedef typename LinkHash::HashTable<K, std::pair<K, V>, mapKeyOFt>::iterator iterator;

		std::pair<iterator, bool> Insert(const std::pair<K, V> kv) {
			return _mp.Insert(kv);
		}
		iterator begin() {
			return _mp.begin();
		}
		iterator end() {
			return _mp.end();
		}
		iterator find(const K& key) {
			return _mp.find(key);
		}
		bool Erase(const K& key) {
			return _mp.Erase(key);
		}
	private:
		LinkHash::HashTable<K, std::pair<K, V>, mapKeyOFt> _mp;
	};

	void test_unordered_map() {
		uonordered_map<int, int> htb;
		htb.Insert({ 0, 0 });
		htb.Insert({ 1, 1 });
		htb.Insert({ 10, 1 });
		htb.Insert({ 11, 1 });
		htb.Insert({ 8, 1 });
		htb.Insert({ 20, 1 });
		htb.Insert({ 22, 1 });
		htb.Insert({ 25, 1 });
		htb.Insert({ 12, 1 });
		htb.Insert({ 38, 1 });
		htb.Insert({ 23, 1 });
		htb.Insert({ 20, 1 });
		auto ret = htb.find(10);
		int retf = ret->first;
		std::cout << retf << std::endl;
		std::cout << htb.Erase(20) << std::endl;
		uonordered_map<int, int> htb1(htb);
		uonordered_map<int, int> htb2;
		htb2.Insert({ 100, 100 });
		htb2 = htb;
		for (auto& e : htb2) {
			std::cout << e.first << " : " << e.second << std::endl;
		}
	}
}
