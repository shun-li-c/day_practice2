#pragma once
#include<vector>
#include"closeHush.h"
//----------------------------------开散列--------------------------------/
//拉链法/哈希桶
//控制负载因子进行增容
namespace LinkHash {
	//哈希表节点
	template<class T>
	struct HushNode {
		T _data;
		HushNode* _next;

		HushNode(const T& data)
			:_data(data)
			, _next(nullptr)
		{}
	};
	//声明
	template<class K, class T, class KeyOFt, class HashFunc>
	class HashTable;

	//迭代器
	template<class K, class T, class Ref, class Ptr, class KeyOFt, class HashFunc = Hush<K>>
	class __HTIterator {
		KeyOFt kot;
		HashFunc hf;
		typedef HushNode<T> Node;
		typedef __HTIterator<K, T, Ref, Ptr, KeyOFt, HashFunc> self;
	private:
		Node* _node;
		HashTable<K, T, KeyOFt, HashFunc>* _pht; //++使用表指针
	public:
		__HTIterator(Node* node, HashTable<K, T, KeyOFt, HashFunc>* pht)
			:_node(node)
			,_pht(pht)
		{}

		Ref operator*() {
			return _node->_data;
		}
		Ptr operator->() {
			return &_node->_data;
		}
		self operator++() {
			if (_node->_next) {
				_node = _node->_next;
			}
			else {
				size_t index = hf(kot(_node->_data)) % _pht->_table.size();
				++index;
				//找下一个不为空的桶
				while (index < _pht->_table.size()) {
					if (_pht->_table[index]) {
						break;
					}
					else {
						++index;
					}
				}
				if (index == _pht->_table.size()) {
					_node = nullptr;
				}
				else {
					_node = _pht->_table[index];
				}
			}
			return *this;
		}
		bool operator!=(const self& s) const {
			return _node != s._node;
		}
		bool operator==(const self& s) const {
			return _node == s._node;
		}
	};

	//哈希表 
	template<class K, class T, class KeyOFt, class HashFunc = Hush<K>>
	class HashTable {

		//声明友元 --> 访问内部 _table
		template<class K, class T, class Ref, class Ptr, class KeyOFt, class HashFunc>
		friend class __HTIterator;

		typedef HashTable<K, T, KeyOFt, HashFunc> self;
		typedef HushNode<T> Node;
		HashFunc hf;
		KeyOFt kot;
	private:
		std::vector<Node*> _table;
		size_t _n = 0;
	public:
		typedef __HTIterator<K, T, T&, T*, KeyOFt, HashFunc> iterator;
	public:
		HashTable() {}
		//拷贝构造
		HashTable(const self& ht) {
			_table.resize(ht._table.size());
			for (size_t i = 0; i < ht._table.size(); i++) {
				Node* cur = ht._table[i];
				while (cur) {
					Node* copy = new Node(cur->_data);
					copy->_next = _table[i];
					_table[i] = copy;

					cur = cur->_next;
				}
			}
		}
		//赋值函数
		self& operator=(self ht) {
			std::swap(_n, ht._n);
			_table.swap(ht._table);
			return *this;
		}
		//析构函数
		~HashTable(){
			for (size_t i = 0; i < _table.size(); i++) {
				Node* cur = _table[i];
				while (cur) {
					Node* next = cur->_next;
					delete cur;
					cur = next;
				}
				_table[i] = nullptr;
			}
		}
		std::pair<iterator, bool> Insert(const T data) {
			auto fid = find(kot(data));
			if (fid != end())
				return std::make_pair(fid, false);
				
			//扩容 -> 负载因子为1时
			if (_n == _table.size()) {
				size_t newSize = _table.size() == 0 ? 10 : _table.size() * 2;
				std::vector<Node*> newTable;
				newTable.resize(newSize);
				for (size_t i = 0; i < _table.size(); i++) {
					Node* cur = _table[i];
					while (cur) {
						Node* next = cur->_next;
						//挪到新表中 -> 头插
						size_t index = hf(kot(cur->_data)) % newTable.size();
						cur->_next = newTable[index];
						newTable[index] = cur;

						cur = next;
					}
				}
				_table.swap(newTable);
			}
			//插入
			Node* newNode = new Node(data);
			size_t index = hf(kot(data)) % _table.size();

			//头插
			newNode->_next = _table[index];
			_table[index] = newNode;
			++_n;
			return std::make_pair(iterator(newNode, this), true);
		}
		iterator find(const K& key) {
			if (_table.size() == 0)
				return end();

			size_t index = hf(key) % _table.size();
			Node* cur = _table[index];
			while (cur) {
				if (kot(cur->_data) == key) {
					return iterator(cur, this);
				}
				cur = cur->_next;
			}
			return end();
		}
		bool Erase(const K& key) {
			if (_table.size() == 0)
				return false;
			size_t index = hf(key) % _table.size();
			Node* cur = _table[index];
			Node* prev = nullptr;
			while (cur) {
				if (kot(cur->_data) == key) {
					//头删,中间
					if (prev == nullptr) {
						_table[index] = cur->_next;
					}
					else {
						prev->_next = cur->_next;
					}
					delete cur;
					--_n;
					return true;
				}
				else {
					prev = cur;
					cur = cur->_next;
				}
			}
			return false;
		}
		iterator begin() {
			for (size_t i = 0; i < _table.size(); i++) {
				if (_table[i])
					return iterator(_table[i], this);
			}
			return end();
		}
		iterator end() {
			return iterator(nullptr, this);
		}
	};

	void test_linkHashTable() {
		//HashTable<int, std::pair<int, int>> htb;
		//htb.Insert({ 0, 0 });
		//htb.Insert({ 1, 1 });
		//htb.Insert({ 10, 1 });
		//htb.Insert({ 11, 1 });
		//htb.Insert({ 8, 1 });
		//htb.Insert({ 20, 1 });
		//htb.Insert({ 22, 1 });
		//htb.Insert({ 25, 1 });
		//htb.Insert({ 12, 1 });
		//htb.Insert({ 38, 1 });
		//htb.Insert({ 23, 1 });
		//htb.Insert({ 20, 1 });
		//std::cout << htb.find(100) << std::endl;
		//std::cout << htb.Erase(20) << std::endl;
	}
}