#pragma once
#include"LinkHash.h"
#include<iostream>
namespace zhu {
	template<class K>
	class unorder_set {
		struct setKeyOFt {
			const K& operator()(const K& key) {
				return key;
			}
		};
	public:
		typedef typename LinkHash::HashTable<K, K, setKeyOFt>::iterator iterator;
	public:
		std::pair<iterator, bool> Insert(const K& key) {
			return _st.Insert(key);
		}
		iterator find(const K& key) {
			return _st.find(key);
		}
		bool erase(const K& key) {
			return _st.Erase(key);
		}
		iterator begin() {
			return _st.begin();
		}
		iterator end() {
			return _st.end();
		}
	private:
		LinkHash::HashTable<K, K, setKeyOFt> _st;
	};
	void test_unordered_set(){
		unorder_set<std::string> ust;
		std::vector<std::string> v{ "222", "asdf", "444", "htfd", "dth" };
		for (auto& vi : v) {
			ust.Insert(vi);
		}
		for (auto& e : ust) {
			std::cout << e << std::endl;
		}
	}
}
