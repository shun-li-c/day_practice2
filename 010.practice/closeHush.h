#pragma once
#include<vector>
#include<string>
//多类型处理(key),增加仿函数
template<class K>
class Hush {
public:
	size_t operator()(const K& key) {
		return key;
	}
};

//特化
template<>
class Hush<std::string> {
public:
	size_t operator()(const std::string& str) {
		size_t val = 0;
		for (auto ch : str) {
			val *= 31;
			val += ch;
		}
		return val;
	}
};
//---------------------------------闭散列----------------------------------/
// 开放定址法 -->映射的位置冲突，按规则找下一个位置存储 --> 除留余数法
//线性探测  、 二次探测
//控制负载因子进行增容
namespace closeHush {
	//标识每个位置状态
	enum class state {
		EXIST,
		DELETE,
		EMPTY
	};
	template<class K, class V>
	struct HushNode {
		std::pair<K, V> _kv;
		state _state = state::EMPTY;
	};
	template<class K, class V, class HashFunc = Hush<K>>
	class HashTable {
		typedef HushNode<K, V> Node;
		HashFunc hf;
	private:
		std::vector<Node> _table;
		size_t _n = 0; //有效数据个数
	public:
		bool Insert(std::pair<K, V> kv) {
			Node* fid = find(kv.first);
			if (fid)
				return false;

			//扩容 --> 载荷因子(表中存在数据/表的长度) >= 0.7 
			if (_table.size() == 0 || _n * 10 / _table.size() >= 7) {
				size_t newSize = _table.size() == 0 ? 10 : _table.size() * 2;

				//防止异地扩容问题,创建新表数据进行挪动
				HashTable<K, V> newTable;
				newTable._table.resize(newSize);
				for (size_t i = 0; i < _table.size(); i++) {
					if (_table[i]._state == state::EXIST) {
						newTable.Insert(_table[i]._kv);
						//递归进行插入
					}
				}
				_table.swap(newTable._table);
			}
			//线性探测
			//size_t start = kv.first % _table.size();
			//size_t i = 0;
			//size_t index = start + i;
			//while (_table[index]._state == state::EXIST) {
			//	++i;
			//	index = start + i;
			//	index %= _table.size();
			//}
			//_table[index]._kv = kv;
			//_table[index]._state = state::EXIST;
			//++_n;

			//二次探测
			size_t start = hf(kv.first) % _table.size();
			size_t i = 0;
			size_t index = start + i * i;
			while (_table[index]._state == state::EXIST) {
				++i;
				index = start + i * i;
				index %= _table.size();
			}
			_table[index]._kv = kv;
			_table[index]._state = state::EXIST;
			++_n;
			return true;
		}
		Node* find(const K& key) {
			if (_table.size() == 0)
				return nullptr;

			size_t start = hf(key) % _table.size();
			size_t i = 0;
			size_t index = start + i;
			while (_table[index]._state != state::EMPTY) {
				if (_table[index]._kv.first == key && _table[index]._state == state::EXIST) {
					return &_table[index];
				}
				++i;
				index = start + i;
				index %= _table.size();
			}
			return nullptr;
		}
		bool Erase(const K& key) {
			Node* fid = find(key);
			if (fid == nullptr) {
				return false;
			}
			else {
				fid->_state = state::DELETE;
				--_n;
				return true;
			}
		}
		void test_closeHashTable() {
			closeHush::HashTable<int, int> htb;
			htb.Insert({ 1, 1 });
			htb.Insert({ 10, 1 });
			htb.Insert({ 11, 1 });
			htb.Insert({ 8, 1 });
			htb.Insert({ 20, 1 });
			htb.Insert({ 22, 1 });
			htb.Insert({ 25, 1 });
			htb.Insert({ 12, 1 });
			htb.Insert({ 38, 1 });
			htb.Insert({ 23, 1 });
			htb.Insert({ 20, 1 });
			std::cout << htb.find(100) << std::endl;
			std::cout << htb.Erase(20) << std::endl;
		}

	};
}


