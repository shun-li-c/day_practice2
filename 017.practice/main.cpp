#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#include<queue>
#include<unordered_map>
using namespace std;

void test() {
	unordered_map<char, int> ump;
	string s = "google";
	for (auto e : s) {
		ump[e]++;
	}
	for (auto e : ump) {
		cout << e.first << " : " << e.second << endl;
	}
}
void test1() {
	deque<int> dq{ 4, 2, 8, 7 };
	cout << (dq.front() + 3) << endl;
}

class Solution {
public:  //{ 2,30,4,2,6,2,5,1 }
    static vector<int> maxInWindows(const vector<int>& num, unsigned int size) {
        vector<int> ret;
        if (num.size() == 0 || size < 1 || num.size() < size) return ret;
        int n = num.size();
        deque<int> dq;
        for (int i = 0; i < n; ++i) {
            while (!dq.empty() && num[dq.back()] < num[i]) {
                dq.pop_back();
            }
            dq.push_back(i);
            // 判断队列的头部的下标是否过期
            if (dq.front() + size <= i) {
                dq.pop_front();
            }
            // 判断是否形成了窗口
            if (i + 1 >= size) {
                ret.push_back(num[dq.front()]);
            }
        }
        return ret;
    }
};
int main() {
	//test();
	//test1();
    vector<int> v{ 2,30,4,2,6,2,5,1 };
    Solution::maxInWindows(v, 3);
	return 0;
}
