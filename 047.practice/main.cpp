#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
using namespace std;
class String {
private:
	char* _str;
	int _size;
public:
	String(const char* str = "")
		:_size(strlen(str))
	{
		_str = new char[_size + 1];
		strcpy(_str, str);
	}

	String(const String& s)
		:_str(nullptr)
	{
		String tmp(s._str);
		swap(tmp._str, _str);	
	}

	~String() {
		if (_str) {
			delete[] _str;
			_str = nullptr;
		}
		_size = 0;
	}
	String& operator=(String s) {
		swap(_str, s._str);
		_size = s._size;
		return *this;
	}
	String& Add(const String& str) {
		int len = strlen(str._str);
		//扩容
		//实际底层是按照二倍扩容，这里些简单点以实际所需进行扩容
		char* tmp = new char[_size + len + 1];
		strcpy(tmp, _str);
		delete[] _str;
		strcpy(tmp + _size, str._str);
		_str = tmp;
		_size = _size + len;
		return *this;
	}
};

struct A {
	union
	{
		int a;
		char b;
	}tmp;
	int a;
	char b;
	float c;

};

void test() {
	String str("asd");
	String tmp("vvv");
	
	String ret = str.Add(tmp);

	////交换两个变量
	//int a = 110, b = 200;
	//a = a ^ b;
	//b = a ^ b;
	//a = a ^ b;

	//cout << a << " " << b << endl;
	//cout << sizeof(A) << endl;
}
namespace zhu
{
	class string
	{
	public:
		string(const char* str = "")
		{
			_str = new char[10];
			strcpy(_str, str);
		}

		~string()
		{
			if (_str)
			{
				delete[] _str;
				_str = nullptr;
			}
		}

		//指针置空和temp中str交换,调用~temp不会释放str空间
		string(const string& s)
			:_str(nullptr)
		{
			string temp(s._str);
			swap(_str, temp._str);
		}

		//利用出作用域s自动销毁来释放原_str空间
		string& operator=(string s)
		{
			swap(_str, s._str);
			return *this;
		}

		//private:
		char* _str;
	};
	void test()
	{

		string s1("hello");
		string s2(s1);
		s1 = s2;
	}
}

void test2() {
	zhu::test();
}
int main() {
	test();
	return 0;
}