#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<string>
#include<unordered_map>
using namespace std;


//class A {
//public:
//	static void printf1(int a) {
//		cout << "A" << a << endl;
//	}
//};
//class B {
//public:
//	virtual void printf2() {
//		cout << "B" << endl;
//	}
//};
//class C :public A,public B {
//public:
//	int a = 0;
//	virtual void printf1() {
//		cout << "C" << endl;
//	}
//	virtual void printf2() {
//		cout << "C" << endl;
//	}
//};
//
//void (A::* p)(int);
//
//void printf(int a) {
//	cout << a << endl;
//}
//void (*pf) (int);
//int main() {
//	C c;
//	A a;
//	pf = printf;
//	//p();
//	pf(11);
//	p = A::printf1;
//	p(11);
//	return 0;
//}



// 声明函数指针
typedef int(*func)(int a, int b);

// 声明一个类
class Calculate {
public:
    // 声明成员函数为静态成员函数，否则在调用的时候无法获取函数地址
    static int Addition(int a, int b) {
        return a + b;
    }
    static int Subtraction(int a, int b) {
        return a - b;
    }
};

// 参数为函数指针，去调用类成员函数
void GetResult(func f)
{
    cout << f(1, 2) << endl;
}

int test()
{
    // 对于类静态成员函数的调用
    //GetResult(&Calculate::Addition);
    //GetResult(&Calculate::Subtraction);
    func f = Calculate::Addition;
    cout << f(1, 2) << endl;
    return 0;
}

int GetUglyNumber_Solution(int index) {
    vector<int> v;
    v.resize(index + 1);
    v[1] = 1;
    if (index == 1) {
        return 1;
    }
    //由题意可知丑数为2^x * 3^y * 5*z
    //若一个数为丑数则x, y, z也为丑数
    //定义v[i]为第i个丑数(从小到大)
    //v[i] = min(min(v[i2]*2, v[i3]*3), v[i5]*5)
    int i2 = 1, i3 = 1, i5 = 1;

    for (int i = 2; i <= index; i++) {
        v[i] = min(min(v[i2] * 2, v[i3] * 3), v[i5] * 5);
        if (v[i] == v[i2] * 2)
            i2++;
        if (v[i] == v[i3] * 3)
            i3++;
        if (v[i] == v[i5] * 5)
            i5++;
    }
    for (auto& e : v) {
        cout << e << " ";
    }
    return v[index];
}

void test1() {
    unordered_map<string, int> ump;
    ump.insert({ "d", 1 });
    ump.insert({ "a", 1 });
    ump.insert({ "v", 1 });
    ump.insert({ "s", 1 });
    ump.insert({ "c", 1 });

    for (auto& e : ump) {
        cout << e.first << " " << e.second << endl;
    }
}
int main() {
    //GetUglyNumber_Solution(100);
    test1();
    return 0;
}



