#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#include<map>
#include<vector>
using namespace std;

void test() {
	char arr[] = { 'a', 'b', 'v', '\0'};
	const char* arr1 = "abv";
	cout << sizeof(arr) << endl;
	cout << strlen(arr) << endl;
	cout << sizeof(arr1) << endl;
	cout << strlen(arr1) << endl;
}
struct A {
	int a;
	char b;
	int c;
};
struct S {
	char c1;
	int i;
	char c2;
};
void test1() {
	//test();
	A a;
	a.a = 10;
	a.b = 'c';
	a.c = 5;
	//cout << sizeof(a) << endl;
	S s;
	s.c1 = 'a';
	s.i = 3;
	s.c2 = 'a';
}

void test2() {
	string s{ "as" };
	string s1{"vc"};
	cout << s << endl;
	int end = s.size();
	s += s1;
	cout << s << endl;
	s.erase(end);
	cout << s << endl;
	string str = "asdf";
	string s2 = str.substr(1, 2);
	cout << s2 << endl;

	map<int, int> mp{ {1, 1}, {2, 2} };
	mp.erase(1);
	
	int a = 10;
}
void test3() {
	vector<int> v{ 1, 2, 3, 4 };
	v.erase(v.begin() + 1);
	int a = 10;
	v.insert(v.begin(), 2);
	vector<pair<int, int>> vp{ {1, 1} , {2, 2} };
	cout << vp[0].first << endl;
	
}

class LRUCache {
private:
	vector<pair<int, int>> vp;
	int benchCpy;
public:
	LRUCache(int capacity) {
		benchCpy = capacity;
	}

	int get(int key) {
		int pos = 0;
		int ret = find(key, pos);
		if (ret == -1) {
			return -1;
		}
		else {
			auto tmp = vp[pos];
			vp.erase(vp.begin() + pos);
			vp.insert(vp.begin(), tmp); //调整位置
			return ret;
		}
	}

	void put(int key, int value) {
		if (vp.capacity() == benchCpy) {
			vp.pop_back();
		}
		vp.insert(vp.begin(), { key, value });
	}

	//返回val值,pos作为输入输出型参数
	int find(int key, int& pos) {
		for (int i = 0; i < vp.size(); i++) {
			if (vp[i].first == key) {
				pos = i;
				return vp[i].second;
			}
		}
		return -1;
	}
};

int main() {
	//test2();
	//test3();
	LRUCache lru(2);
	cout << lru.get(2) << endl;;
	lru.put(2, 6);
	cout << lru.get(1) << endl;
	lru.put(1, 5);
	lru.put(1, 2);
	cout << lru.get(1) << endl;
	cout << lru.get(2) << endl;

	return 0;
}
