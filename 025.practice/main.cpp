#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

//字符串操作式加法
bool Increment(char* number) {

	int flag = 0; //进位标志
	int nLength = strlen(number);
	for (int i = nLength - 1; i >= 0; i--) { //从字符串最后一位开始++
		int nSum = number[i] - '0' + flag;
		if (i == nLength - 1) {
			nSum++;
		}
		if (nSum >= 10) {
			if (i == 0) {
				return true;
			}
			else {
				nSum -= 10;
				flag = 1;
				number[i] = '0' + nSum; //需要进位，先清零
			}

		}
		else {
			number[i] = '0' + nSum;
			break;
		}
	}
	return false;
}

//void PrintfNumber(char* number) {
//	bool IsBegin = true;
//	int nLength = strlen(number);
//	for (int i = 0; i < nLength; ++i) {
//		if (IsBegin && number[i] != '0')
//			IsBegin = false;
//		if (!IsBegin) {
//			printf("%c", number[i]);
//		}
//	}
//	printf(" ");
//}
void Printf1ToMaxOfNDigits(int n) {
	if (n <= 0) {
		return ;
	}
	vector<int> v;
	char* number = new char[n + 1];
	memset(number, '0', n);
	number[n] = '\0';

	while (!Increment(number)) {
		//PrintfNumber(number);
		//v.push_back(atoi(number));
		cout << number << " ";
	}
	delete[] number;
	for (auto& val : v) {
		cout << val << " ";
	}
}


//是否为奇数
bool oddNumber(int n) {
	return n % 2 == 0 ? false : true;
}
vector<int> reOrderArray(vector<int>& array) {
	//左找偶数，右找奇数互换
	int left = 0, right = array.size() - 1;
	while (left < right) {
		while (left < right && oddNumber(array[left])) {
			left++;
		}

		while (left < right && !oddNumber(array[right])) {
			right--;
		}

		swap(array[left], array[right]);
	}
	return array;
}

int main() {
	//Printf1ToMaxOfNDigits(2);
	vector<int> v = { 1, 2, 3, 4 };
	reOrderArray(v);
	return 0;
}
