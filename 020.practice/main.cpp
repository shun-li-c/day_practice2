#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

void _MergeSort(int* arr, int* copy, int begin, int end) {
	//退出条件
	if (begin >= end) {
		return;
	}
	int mid = (begin + end) >> 1;
	int begin1 = begin, end1 = mid;
	int begin2 = mid + 1, end2 = end;
	_MergeSort(arr, copy, begin1, end1);
	_MergeSort(arr, copy, begin2, end2);

	//排序
	int index = begin;
	while (begin1 <= end1 && begin2 <= end2) {
		if (arr[begin1] < arr[begin2])
			copy[index++] = arr[begin1++];
		else {
			copy[index++] = arr[begin2++];
		}
	}

	//把剩余元素挪到copy数组中
	while (begin1 <= end1) {
		copy[index++] = arr[begin1++];
	}

	while (begin2 <= end2) {
		copy[index++] = arr[begin2++];
	}

	//拷贝回原数组
	for (int i = begin; i <= end; i++) {
		arr[i] = copy[i];
	}
}

void MergeSort(int* arr, int n) {
	int* copy = (int*)malloc(sizeof(int) * n);
	if (copy == nullptr) {
		perror("malloc error");
		exit(-1);
	}
	_MergeSort(arr, copy, 0, n - 1);
}

class Solution{
public:
    int InversePairs(vector<int> data) {
        if (data.size() < 2)
            return 0;
        vector<int> tmp(data);
        int count = _InversePairs(data, tmp, 0, data.size() - 1);
        return count;
    }
    long long _InversePairs(vector<int>&data, vector<int>&tmp, int begin, int end)
    {
        if (begin == end)
        {
            tmp[begin] = data[begin];
            return 0;
        }
        //int mid = (begin + end) >> 1;
        int mid = (end - begin) >> 1;
        long long left = _InversePairs(tmp, data, begin, begin + mid);
        long long right = _InversePairs(tmp, data, begin + mid + 1, end);

        //将i初始化为前半段最后一个位置下标
        int i = begin + mid;
        //将j初始化为后半段最后一个位置下标
        int j = end;
        long long count = 0;
        int indexTmp = end;
        while (i >= begin && j >= begin + mid + 1)
        {
            if (data[i] > data[j])
            {
                tmp[indexTmp--] = data[i--];
                count += (j - begin - mid);
            }
            else
            {
                tmp[indexTmp--] = data[j--];
            }
        }
        for (; i >= begin; i--)
        {
            tmp[indexTmp--] = data[i];
        }
        for (; j >= begin + mid + 1; j--)
        {
            tmp[indexTmp--] = data[j];
        }
        return (count + left + right) % 1000000007;
    }
};

class Solution1 {
private:
    const int kmod = 1000000007;
public:
    int InversePairs(vector<int> data) {
        int ret = 0;
        merge_sort__(data, 0, data.size() - 1, ret);
        return ret;
    }


    void merge_sort__(vector<int>& arr, int l, int r, int& ret) {
        if (l >= r) {
            return;
        }

        int mid = l + ((r - l) >> 1);
        merge_sort__(arr, l, mid, ret);
        merge_sort__(arr, mid + 1, r, ret);
        merge__(arr, l, mid, r, ret);
    }

    void merge__(vector<int>& arr, int l, int mid, int r, int& ret) {
        vector<int> tmp(r - l + 1);
        int i = l, j = mid + 1, k = 0;

        while (i <= mid && j <= r) {
            if (arr[i] > arr[j]) {
                tmp[k++] = arr[j++];
                // 奥妙之处
                ret += (mid - i + 1);
                ret %= kmod;
            }
            else {
                tmp[k++] = arr[i++];
            }
        }

        while (i <= mid) {
            tmp[k++] = arr[i++];
        }
        while (j <= r) {
            tmp[k++] = arr[j++];
        }

        for (k = 0, i = l; i <= r; ++i, ++k) {
            arr[i] = tmp[k];
        }
    }
};

int main() {
	//int arr[] = { 6, 3, 9, 30, 88, 34, 23, 100 };
	//MergeSort(arr, sizeof(arr) / sizeof(arr[0]));
    Solution sl;
    vector<int> arr{ 4, 5, 1, 2 };
    //vector<int> copy(arr.size());
    int ret = sl.InversePairs(arr);
    cout << ret << endl;
    return 0;
}

