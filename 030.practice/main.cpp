#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

class A {
public:
	virtual void Printf() {
		cout << "A" << endl;
	}
	 void whoami() {
		Printf();
	}


};
class B : public A{
public:
	virtual void Printf() {
		cout << "B" << endl;
	}
	//void whoami() {
	//	Printf();
	//}
};

void test() {
	vector<int> v{1, 2, 2, 2, 2, 3};
	vector<int>::iterator it = v.begin();
	while (it != v.end()) {
		if (*it == 2) {
			it = v.erase(it);
			continue;
		}
		++it;
	}
	for (auto& e : v) {
		cout << e << " ";
	}
}

void test1() {
	shared_ptr<int> p1(new int(10));
	cout << *p1 << endl;

	shared_ptr<int> p2 = make_shared<int>(110);
	cout << *p2 << endl;
}
void test2() {
	bool flag = true;
	flag = !flag;
	cout << flag;
}
int main() {
	//B b;
	//b.whoami();
	//b.A::Printf();
	//test1();
	test2();
	return 0;
}