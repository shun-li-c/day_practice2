#pragma once

typedef char BTNodeType;
class BTNode {
public:
    BTNode* left;
    BTNode* right;
    BTNodeType data;
};


//创造节点
BTNode* CreateNode(BTNodeType x);
//前序
void PrevOder(BTNode* root);
//中序
void InOder(BTNode* root);
//后序
void PostOder(BTNode* root);
//层序
void LevelOder(BTNode* root);
//节点个数
size_t TreeSize(BTNode* root);
//叶子节点个数
size_t LeafSize(BTNode* root);
//查找K层个数
size_t TreeLevelSize(BTNode* root, int k);
//查找树中值为x的节点并返回地址
BTNode* TreeFind(BTNode* root, BTNodeType x);
//是否为完全二叉树
bool BinaryTreeComplate(BTNode* root);


