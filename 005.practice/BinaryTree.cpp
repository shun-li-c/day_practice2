#define _CRT_SECURE_NO_WARNINGS 1
#include"BinaryTree.h"
#include<queue>
#include<assert.h>
#include<iostream>
//创造节点
BTNode* CreateNode(BTNodeType x) {
	BTNode* node = new BTNode;
	node->left = nullptr;
	node->right = nullptr;
	node->data = x;
	return node;
}
//前序
void PrevOder(BTNode* root) {
	if (root == nullptr)
		return;
	std::cout << root->data << " ";
	PrevOder(root->left);
	PrevOder(root->right);
}
//中序
void InOder(BTNode* root) {
	if (root == nullptr)
		return;
	InOder(root->left);
	std::cout << root->data << " ";
	InOder(root->right);
}
//后序
void PostOder(BTNode* root) {
	if (root == nullptr)
		return;
	PostOder(root->left);
	PostOder(root->right);
	std::cout << root->data << " ";
}
//层序
void LevelOder(BTNode* root) {
	//借助队列来进行操作
	std::queue<BTNode*> st;
	if(root)
		st.push(root);
	while (!st.empty())
	{
		BTNode* PopNode = st.front();
		st.pop();
		std::cout << PopNode->data << " ";
		if (PopNode->left)
			st.push(PopNode->left);
		if (PopNode->right)
			st.push(PopNode->right);
	}
	std::cout << std::endl;
}
//节点个数
size_t TreeSize(BTNode* root) {
	if (root == nullptr)
	{
		return 0;
	}
	return 1 + TreeSize(root->left) + TreeSize(root->right);
}
//叶子节点个数
size_t LeafSize(BTNode* root){
	if (root == nullptr)
	{
		return 0;
	}
	else if (root->left == nullptr && root->right == nullptr)
	{
		return 1;
	}
	return LeafSize(root->left) + LeafSize(root->right);
}
//查找K层个数
size_t TreeLevelSize(BTNode* root, int k) {
	if (root == nullptr)
	{
		return 0;
	}
	if (k == 1)
	{
		return 1;
	}
	return TreeLevelSize(root->left, k - 1) 
		 + TreeLevelSize(root->left, k - 1);
}
//查找树中值为x的节点并返回地址
BTNode* TreeFind(BTNode* root, BTNodeType x) {
	if (root == nullptr)
		return nullptr;
	if (root->data == x) {
		return root;
	}
	BTNode* LeftRet = TreeFind(root->left, x);
	if (LeftRet)
		return LeftRet;
	BTNode* RightRet = TreeFind(root->right, x);
	if (RightRet)
		return RightRet;
	return nullptr;
}
//是否为完全二叉树
bool BinaryTreeComplate(BTNode* root) {
	//像层序遍历一样,利用队列不过要把空节点入队列
	std::queue<BTNode*> qe;
	if (root)
		qe.push(root);
	while (!qe.empty())
	{
		BTNode* front = qe.front();
		qe.pop();
		if (front == nullptr)
			break;
		qe.push(front->left);
		qe.push(front->right);
	}
	while (!qe.empty())
	{
		BTNode* front = qe.front();
		qe.pop();
		if (front)
			return false;
	}
	return true;
}
