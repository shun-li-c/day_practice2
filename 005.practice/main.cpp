#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include"BinaryTree.h"
using namespace std;

void test() {
    BTNode* A = CreateNode('A');
    BTNode* B = CreateNode('B');
    BTNode* C = CreateNode('C');
    BTNode* D = CreateNode('D');
    BTNode* E = CreateNode('E');
    BTNode* F = CreateNode('F');
    BTNode* G = CreateNode('G');

    A->left = B;
    A->right = C;
    B->left = D;
    B->right = E;
    C->left = F;
    C->right = G;
    PrevOder(A);
    cout << endl;
    InOder(A);
    cout << endl;
    PostOder(A);
    cout << endl;
    LevelOder(A);
    cout << endl;
    cout << TreeSize(A) << endl;
    cout << LeafSize(A) << endl;
    cout << TreeLevelSize(A, 3) << endl;
    cout << TreeFind(A, 'F') << endl;
    cout << BinaryTreeComplate(A) << endl;
}
int main() {
    test();
    return 0;
}

