#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<string>
using namespace std;

void _MergeSortCPP(vector<int>& v, vector<int>& retV, int left, int right) {
	if (left >= right) {
		return;
	}
	int mid = (left + right) >> 1;
	int begin1 = left, end1 = mid;
	int begin2 = mid + 1, end2 = right;
	_MergeSortCPP(v, retV, begin1, end1);
	_MergeSortCPP(v, retV, begin2, end2);

	int indexV = left;
	while (begin1 <= end1 && begin2 <= end2) {
		if (v[begin1] < v[begin2]) {
			retV[indexV++] = v[begin1++];
		}
		else {
			retV[indexV++] = v[begin2++];
		}
	}

	while (begin1 <= end1) {
		retV[indexV++] = v[begin1++];
	}

	while (begin2 <= end2) {
		retV[indexV++] = v[begin2++];
	}

	//将数据挪回原数组
	for (int i = left; i <= right; i++) {
		v[i] = retV[i];
	}
}

//归并排序
void MergeSortCPP(vector<int>& v) {
	vector<int> retV(v.size());
	_MergeSortCPP(v, retV, 0, v.size() - 1);
}

void testCPP() {
	vector<int> v{ 1, 7, 2, 9, 3, 0, 44, 55, 11, 22 };
	MergeSortCPP(v);
	for (auto& val : v) {
		cout << val << " ";
	}
	cout << endl;
}

void _MergeSortC(int* arr, int* temp, int left, int right) {
	if (left >= right) {
		return;
	}

	int mid = (left + right) >> 1;
	int begin1 = left, end1 = mid;
	int begin2 = mid + 1, end2 = right;
	_MergeSortC(arr, temp, begin1, end1);
	_MergeSortC(arr, temp, begin2, end2);

	int index_T = left;
	while (begin1 <= end1 && begin2 <= end2) {
		if (arr[begin1] < arr[begin2]) {
			temp[index_T++] = arr[begin1++];
		}
		else {
			temp[index_T++] = arr[begin2++];
		}
	}

	while(begin1 <= end1)
		temp[index_T++] = arr[begin1++];
	while(begin2 <= end2)
		temp[index_T++] = arr[begin2++];

	//将排好序数组放回原数组中去
	for (int i = left; i <= right; i++) {
		arr[i] = temp[i];
	}

}
void MergeSortC(int* arr, int n) {
	int* temp = (int*)malloc(sizeof(int) * n);
	if (temp == nullptr) {
		perror("MergeSortC malloc error");
		exit(-1);
	}
	_MergeSortC(arr, temp, 0, n - 1);
	free(temp);
}

void testC() {
	int arr[] = { 1, 7, 2, 9, 3, 0, 44, 55, 11, 22 };
	MergeSortC(arr, sizeof(arr) / sizeof(arr[0]));
	for (auto& val : arr) {
		cout << val << " ";
	}
	cout << endl;
}
void test1() {
	string str = "asdf";
	//str.resize(1);
	str.reserve(20);
	//str[1] = 'a';
	cout << str.size() << endl;
	cout << str.capacity() << endl;
}


//-------------------------最长回文字符串---------------------------/
class Solution {
public:
	static string longestPalindrome(string s) {
		// sz代表字符串长度，剩下变量分别用于记录最长回文子串起始点以及最大长度
		int sz = s.size();
		int max_start = 0;
		int max_len = 1;
		// 开一个行和列长度都为sz的bool类型的二维数组，记录子串是否满足回文
		vector<vector<bool>> dp(sz, vector<bool>(sz));
		// 从第字符串二个位置开始，从头往后找到这个位置，判断路径上的串是否回文
		for (int j = 1; j < sz; ++j)
		{
			for (int i = 0; i < j; ++i)
			{
				// 若发现最左和最右满足回文条件
				if (s[i] == s[j])
				{
					// 若其长度等于2或者3，则直接判定为回文串，令其为true
					if (j - i == 1 || j - i == 2)
						dp[i][j] = true;
					else
					{   // 若长度大于3，则看看除开左右两头的，中间子串是否为回文
						if (dp[i + 1][j - 1] == false)
							continue;
						// 子串为回文，则当前串也为回文，令其为true
						dp[i][j] = true;
					}
					// 若当前串为回文，而且长度大于最大长度，则更新起点与及长度
					if (dp[i][j] == true && (j - i + 1) > max_len)
					{
						max_len = j - i + 1;
						max_start = i;
					}
				}
			}
		}
		for (int i = 0; i < dp.size(); i++) {
			for (int j = 0; j < dp[0].size(); j++) {
				cout << dp[i][j] << " ";
			}
			cout << endl;
		}
		return s.substr(max_start, max_len);

	}
};
void test2() {
	string str("assadsfdv");
	cout << Solution::longestPalindrome(str) << endl;
}
int main() {
	//testCPP();
	//testC();
	//test1();
	test2();
	return 0;
}
