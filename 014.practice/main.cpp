#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>

//-----------------------二叉搜索树最近公共祖先-------------------/
struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};
 
int main() {

    //方法一:递归
    class Solution {
    public:
        int lowestCommonAncestor(TreeNode* root, int p, int q) {
            if (root == nullptr)
                return 0;
            if (p >= root->val && q <= root->val || q >= root->val && p <= root->val)
                return root->val;
            else if (p >= root->val && q >= root->val)
                return lowestCommonAncestor(root->right, p, q);
            else //if(p <= root->val && q <= root->val)
                return lowestCommonAncestor(root->left, p, q);

        }
    };

    //方法二:保存路径,找两个路径最后相同那个节点即为公共祖先
	return 0;
}

