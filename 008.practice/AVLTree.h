#pragma once
#include<iostream>
#include<assert.h>
namespace zhu {
	template<class K, class V>
	class AVLTreeNode {
	public:
		AVLTreeNode* _left;
		AVLTreeNode* _right;
		AVLTreeNode* _parent;
		std::pair<K, V> _KV;
		int _bf; //blance factor
	
		AVLTreeNode(std::pair<K, V> kv)
			:_left(nullptr)
			, _right(nullptr)
			, _parent(nullptr)
			, _KV(kv)
			,_bf(0)
		{}
	};

	template<class K, class V>
	class AVLTree {
	private:
		typedef AVLTreeNode<K, V> Node;
		Node* _root;
	public:
		AVLTree()
			:_root(nullptr)
		{}
		bool insert(const std::pair<K, V> kv) {
			//先向搜索二叉树一样插入
			if (_root == nullptr) {
				_root = new Node(kv);
				return true;
			}
			Node* parent = nullptr;
			Node* cur = _root;
			while (cur) {
				if (cur->_KV.first < kv.first) {
					parent = cur;
					cur = cur->_right;
				}
				else if(cur->_KV.first > kv.first) {
					parent = cur;
					cur = cur->_left;
				}
				else {
					return false;
				}
			}
			cur = new Node(kv);
			if (parent->_KV.first < kv.first) {
				parent->_right = cur;
				cur->_parent = parent;
			}
			else if (parent->_KV.first > kv.first) {
				parent->_left = cur;
				cur->_parent = parent;
			}

			//控制平衡
			//1、更新平衡因子
			//2、出现异常平衡因子，旋转处理
			while (parent) {
				if (cur == parent->_left)
					parent->_bf--;
				else if (cur == parent->_right)
					parent->_bf++;
				if (parent->_bf == 0)
					break;
				else if (parent->_bf == 1 || parent->_bf == -1) {
					//继续向上更新
					cur = parent;
					parent = parent->_parent;
				}
				else if (parent->_bf == 2 || parent->_bf == -2) {
					//旋转处理
					if (parent->_bf == 2 && cur->_bf == 1) {
						//左单旋
						RotateL(parent);
					}
					if (parent->_bf == -2 && cur->_bf == -1) {
						//右单旋
						RotateR(parent);
					}
					if (parent->_bf == 2 && cur->_bf == -1) {
						//右左旋
						RotateRL(parent);
					}
					if (parent->_bf == -2 && cur->_bf == 1) {
						//左右旋
						RotateLR(parent);
					}
				}
				else {
					//说明插入更新平衡因子之前，平衡因子就有问题了
					assert(false);
				}
			}
			return true;
		}
		void Inorder() {
			_Iorder(_root);
		}
		void IsBalance() {
			_IsBalance(_root);
		}
	private:
		void RotateL(Node* parent) {
			Node* subR = parent->_right;
			Node* subRL = subR->_left;

			parent->_right = subRL;
			if (subRL)
				subRL->_parent = parent;
			Node* grandParent = parent->_parent;

			subR->_left = parent;
			parent->_parent = subR;
			if (parent == _root) {
				_root = subR;
				_root->_parent = nullptr;
			}
			else {
				if (grandParent->_left == parent) {
					grandParent->_left = subR;
				}
				else if (grandParent->_right == parent) {
					grandParent->_right = subR;
				}
				subR->_parent = grandParent;

			}
			subR->_bf = parent->_bf = 0;
		}
		void RotateR(Node* parent) {
			Node* subL = parent->_left;
			Node* subLR = subL->_right;

			parent->_left = subLR;
			if (subLR)
				subLR->_parent = parent;
			Node* grandParent = parent->_parent;

			subL->_right = parent;
			parent->_parent = subL;

			if (parent == _root) {
				_root = subL;
				_root->_parent = nullptr;
			}
			else {
				if (grandParent->_left == parent) {
					grandParent->_left = subL;
				}
				else if (grandParent->_right == parent) {
					grandParent->_right = subL;
				}
				subL->_parent = grandParent;
			}
			subL->_bf = parent->_bf = 0;
		}
		void RotateLR(Node* parent) {
			RotateL(parent->_left);
			RotateR(parent);
		}
		void RotateRL(Node* parent) {
			Node* subR = parent->_right;
			Node* subRL = subR->_left;
			int bf = subRL->_bf;

			RotateR(parent->_right);
			RotateL(parent);
			if (bf == 0) {
				parent->_bf = subR->_bf = subRL->_bf = 0;
			}
			else if (bf == 1) {
				parent->_bf = -1;
				subR->_bf = subRL->_bf = 0;
			}
			else if (bf == -1) {
				subR->_bf = 1;
				parent->_bf = subRL->_bf = 0;
			}
			else {
				assert(false);
			}
		}
		void _Iorder(Node* root) {
			if (root == nullptr) {
				return;
			}
			_Iorder(root->_left);
			std::cout << root->_KV.first << " : " << root->_KV.second << std::endl;
			_Iorder(root->_right);
		}
		int Height(Node* root) {
			if (root == nullptr) {
				return 0;
			}
			int leftHeight = Height(root->_left);
			int rightHeight = Height(root->_right);

			return leftHeight > rightHeight ? leftHeight + 1 : rightHeight + 1;
		}
		bool _IsBalance(Node* root) {
			if (root == nullptr) {
				return true;
			}
			int leftHeight = Height(root->_left);
			int rightHeight = Height(root->_right);
			
			if ((rightHeight - leftHeight) != root->_bf) {
				std::cout << root->_KV.first << " 现在是: " << root->_bf << std::endl;
				std::cout << root->_KV.first << " 应该是: " << rightHeight - leftHeight << std::endl;
			}
			return abs(leftHeight - rightHeight) < 2
				&& _IsBalance(root->_left)
				&& _IsBalance(root->_right);
		}
	};
	void testAVLTree() {
		AVLTree<int, int> t1;
		t1.insert({ 1, 1 });
		t1.insert({ 5, 1 });
		t1.insert({ 2, 1 });
		t1.insert({ 3, 1 });
		t1.insert({ 4, 1 });
		t1.insert({ 6, 1 });
		t1.insert({ 61, 1 });
		t1.insert({ 16, 1 });
		t1.insert({ 36, 1 });
		t1.insert({ 26, 1 });
		//t1.insert({ 56, 1 });
		//t1.insert({ 66, 1 });
		t1.Inorder();
		t1.IsBalance();

		//注: 左右双旋平衡因子没有处理
	}
}














