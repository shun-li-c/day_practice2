#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

void test4()
{
	int a[4] = { 1, 2, 3, 4 };
	int* ptr1 = (int*)(&a + 1);
	int* ptr2 = (int*)((int)a + 1);	  //存储 01 00 00 00 02 00 00 00 03 00 00 00 04 00 00 00
									  //把地址转换成整型 + 1 -- 在转换成 int型指针
									  //即从左边第二个字节开始打印

	printf("%x,%x", ptr1[-1], *ptr2); // 4 ， 2000000

}

//在左旋数字查找值
int find(vector<int> arr, int target, int left, int right) {
	while (left <= right) {
		int mid = (left + right) >> 1;
		if (arr[mid] > target ) {
			if (arr[mid] > arr[left])
				right = mid - 1;
			else
				left = mid + 1;
		}
		else if (arr[mid] < target ) {
			if (arr[mid] > arr[left])
				left = mid + 1;
			else
				right--;
			
		}
		if (arr[left] == target)
			return left;
		if (arr[mid] == target)
			return mid;
	}
	return -1;
}
int main() {
	//test4();
	vector<int> arr{ 4, 5, 6, 7, 0, 1 };
	cout << find(arr, 0, 0, arr.size() - 1) << endl;
	return 0;
}
