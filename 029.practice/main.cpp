#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

class A {
public:
	int _a = 0;
public:
	A(int a)
	  :_a(a)
	{}
};
int test() {
	//vector<> 迭代器不能删除,涉及迭代器失效问题
	vector<int> v{2, 4, 7, 6, 5, 1};
	vector<int>::iterator it = v.begin();
	vector<int>::iterator era = v.begin();
	//while (it != v.end()) {
	//	if (*it == 7) {
	//		era = it;
	//		++it;
	//		v.erase(it);
	//		continue;
	//	}
	//		
	//	it++;
	//}

	char* str = new char[10];
	memset(str, 0, 10);

	A a(10);
	A* pa = new A(20);
	cout << pa->_a << endl;

	cout << endl;
	int aa = 10;
	int bb = -10;
	auto Add = [&]() {aa = 100, bb = 90; };
	//cout << Add(10, 20) << endl;
	//cout << Add() << endl;
	Add();
	cout << aa <<  " " << bb << endl;
	return 0;
}

int GetSumCount(int n, int sum) {
	if (n < 1 || sum < n || sum > 6 * n)
		return 0;
	if (n == 1)
		return 1;
	int retCount = 0;
	retCount = GetSumCount(n - 1, sum - 1) + GetSumCount(n - 1, sum - 2)
		+ GetSumCount(n - 1, sum - 3) + GetSumCount(n - 1, sum - 4)
		+ GetSumCount(n - 1, sum - 5) + GetSumCount(n - 1, sum - 6);
	return retCount;
}

int test2() {
	//cout << GetSumCount(2, 3) << endl;
	// n 骰子数目
	int n = 4;
	int total = pow((float)6, n);
	for (int i = n; i <= 6 * n; ++i)
	{
		float ratio = (float)GetSumCount(n, i) / total;
		printf("%d: %f\n", i, ratio);
	}
	cout << INT_MIN << endl;
	return 0;
}

class Sum final{
private:
	static int _count;
	static int _get_sum;
public:
	Sum() {
		_count++;
		_get_sum += _count;
	}
	static int GetSum() {
		return _get_sum;
	}
};

int Sum::_count = 0;

#include <Windows.h>
#include <process.h>
#include<thread>
#include <stdio.h>
//静态TLS
static _declspec(thread) int iTest = 0;
unsigned int ThreadProc(int pvParam)
{
	iTest = 3;
	printf("0x%x：%d\n", &iTest, iTest);
	return 0;
}

int test3()
{
	iTest = 5;
	printf("0x%x：%d\n", &iTest, iTest);
	thread t1(ThreadProc, iTest);
	thread t2(ThreadProc, iTest);

	t1.join();
	t2.join();
	Sleep(100);
	return 0;
}

void test4() {
	string str = "maxhub\0\0\x1//\"";
	cout << str.size() << endl;
	//cout << strlen(str) << endl;
}
int main() {
	//test3();
	test4();
	return 0;
}