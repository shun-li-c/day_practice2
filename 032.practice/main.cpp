#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#include<vector>
#include<unordered_map>
using namespace std;


void Conut(int n) {
    int _count = 0;
    for (int i = 6; i <= n; i++) {
        int temp = 0;
        for (int j = 1; j < i; j++) {
            if (i % j == 0) {
                temp += j;
            }
        }
        if (temp == i)
            _count++;
    }
    cout << _count << endl;
}
void test1() {
    int n;
    while (cin >> n) { // 注意 while 处理多个 case
          Conut(n);
    }
}

class Solution1 {
public:
    static string convert(string s, int numRows) {
        //二维矩阵方法
        int row = numRows, col = s.size() / numRows;
        col = col + (col - 1) * (numRows - 2);
        //cout << row << " " << col << endl;
        vector<vector<string>> vv(numRows, vector<string>(col, " "));
        int index = 0;
        int i = 0, j = 0; //控制矩阵行和列
        bool flag = false;
        while (i < row && j < col && index < s.size()) {
            //if (i < 0 || j < 0) {
            //    int a = 0;
            //}
            vv[i][j] = s[index++];
            if (flag)
                i--;
            else
                i++;
            if (i == row || i == 0) {
                j++;
                flag = !flag;
                if (i == row)
                    i = row - 2;
            }
        }
        string ret_str;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (vv[i][j] != " ")
                    //ret_str += vv[i][j];
                    cout << vv[i][j];
            }
            cout << endl;
        }
        return ret_str;
    }
};
void test2() {
    string str{"PAYPALISHIRING"};
    Solution1::convert(str, 3);
}

void test3() {
    string str{ "123" };
    cout << atoi(str.c_str()) << endl;
    INT_MAX;
    INT_MIN;
}
class Solution {
public:
    static int reverse(int x) {
        int ret = 0;
        while (x) {

            int temp = x % 10;
            if (ret > INT_MAX || ret == INT_MAX / 10 && temp > (INT_MAX % 10))
                return 0;
            if (ret < INT_MIN || ret == INT_MIN / 10 && temp < (INT_MIN % 10))
                return 0;

            ret = ret * 10 + temp;
            x /= 10;

        }
        return ret;
    }
};
void test4() {
    cout << Solution::reverse(100) << endl;
}
int main() {
    //test2();
    //test3();
    test4();
    return 0;
}

