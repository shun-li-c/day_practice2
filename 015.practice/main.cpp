#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
#include<vector>
using namespace std;


struct TreeNode {
 	int val;
 	struct TreeNode *left;
	struct TreeNode *right;
 	TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};
//--------------------二叉树中和为某值的路径--------------------------/

class Solution1 {
public:
    void _FindPath(TreeNode* root, int sum, vector<int> v, int& nPath) {
        if (root == nullptr) {
            return;
        }
        v.push_back(root->val);
        if (root->left == nullptr && root->right == nullptr) {
            //到了根节点开始查看路径
            for (int i = 0; i < v.size(); i++) {
                int temp = 0;
                for (int j = i; j < v.size(); j++) {
                    temp += v[j];
                    if (temp == sum) {
                        nPath++;
                        for (int k = i; k <= j; k++) {
                            cout << v[k] << ":" << k << " ";
                        }
                        cout << endl;
                        break;
                    }
                }
            }
            return;
        }
        _FindPath(root->left, sum, v, nPath);
        _FindPath(root->right, sum, v, nPath);
    }
    int FindPath(TreeNode* root, int sum) {
        if (root == nullptr) {
            return 0;
        }
        vector<int> v;
        int nPath = 0;
        _FindPath(root, sum, v, nPath);
        return nPath;
    }
};
#include <unordered_map>

class Solution {
public:
    //方法一:两次遍历
    // int res = 0;
    // void dfs(TreeNode* root, int sum){
    //     if(root == nullptr){
    //         return;
    //     }
    //     if(sum == root->val){
    //         res++;
    //     }
    //     dfs(root->left, sum - root->val);
    //     dfs(root->right, sum - root->val);
    // }
    // int FindPath(TreeNode* root, int sum) {
    //     if(root == nullptr){
    //         return res;
    //     }
    //     dfs(root, sum);
    //     FindPath(root->left, sum);
    //     FindPath(root->right, sum);
    //     return res;
    // }

    unordered_map<int, int> _ump; //ump中存储的是从根节点到该节点前的所有可能累加和
    //如最左侧时ump中插入的值 {{0, 1}, {1, 1},{3, 1}{7, 1}} --> _ump[temp]++
    //方法二:遍历 + 哈希
    int dfs(TreeNode* root, int sum, int bench) {
        if (root == nullptr) {
            return 0;
        }

        int ret = 0; //返回路径数
        int temp = bench + root->val;
        //如果基准值减去sum的值在哈希表中出现过，说明该路径存在ret+哈希中val值
        if (_ump.find(temp - sum) != _ump.end()) {
            //加上前面有的路径数
            ret += _ump[temp - sum];
        }

        _ump[temp]++; //将到该节点的和记录下来

        ret += dfs(root->left, sum, temp);
        ret += dfs(root->right, sum, temp);
        //回退该路径和,因为别的树枝不需要这边的路径和
        _ump[temp]--;
        return ret;
    }

    int FindPath(TreeNode* root, int sum) {
        if (root == nullptr) {
            return 0;
        }
        _ump[0] = 1;
        return dfs(root, sum, 0);
    }
};
int main() {
 
	return 0;
}
