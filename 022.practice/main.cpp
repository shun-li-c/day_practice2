#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
#include<thread>
#include<boost>
using namespace std;
//class Person
//{
//public:
//	void Print()
//	{
//		cout << "name:" << _name << endl;
//		cout << "age:" << _age << endl;
//	}
//protected:
//	string _name = "peter"; // ����
//	int _age = 18; // ����
//};
//class Student : public Person
//{
//protected:
//	int _stuid; // ѧ��
//};
//class Teacher : public Person
//{
//protected:
//	int _jobid; // ����
//};
//
//class A
//{
//public:
//	int _a;
//};
//// class B : public A
//class B : virtual public A
//{
//public:
//	int _b;
//};
//// class C : public A
//class C : virtual public A
//{
//public:
//	int _c;
//};
//class D : public B, public C
//{
//public:
//	int _d;
//};

//int main() {
//	//Student s;
//	//Teacher t;
//	//s.Print();
//	//t.Print();
//
//	//D d;
//	//d.B::_a = 1;
//	//d.C::_a = 2;
//	//d._b = 3;
//	//d._c = 4;
//	//d._d = 5;
//
//	cout << sizeof(A) << endl;
//	cout << sizeof(B) << endl;
//	cout << sizeof(C) << endl;
//	cout << sizeof(D) << endl;
//	return 0;
//}

struct F {
public:
	void foo() { printf("foo"); }
	virtual void bar() { printf("bar"); };
	F() { bar(); }
};

struct J :public F{
public:
	void foo() { printf("b_foo"); }
	void bar() { printf("b_bar"); }
};

//int main() {
//	F* p = new J;
//	p->foo();
//	p->bar();
//	return 0;
//}

class A
{
public:
	void print()
	{
		cout << "A:print()";
	}
};
class B : private A
{
public:
	void print()
	{
		cout << "B:print()";
	}
};
class C : public B
{
public:
	void print()
	{
		//A::print();
		//B::print();
	}
};
//int main()
//{
//	C b;
//	b.print();
//}

int max_count = 100000000;
int counter_a = 0;
int counter_b = 0;

void thread_proc_a() {
	for (int i = 0; i < max_count; i++) {
		counter_a += rand();
	}
}
void thread_proc_b() {
	for (int i = 0; i < max_count; i++) {
		counter_b += rand();
	}
}
int main() {
	boost::thread_grop tg;

	srand(time(nullptr));

	tg.create_thread(thread_proc_a);
	tg.create_thread(thread_proc_b);

	tg.join_all();
}