#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
#include<queue>
#include<vector>
#include<unordered_map>

using namespace std;
class Cmp {
public:
	bool operator()(int a, int b) {
		return a > b;
	}	
};

void test_priority_queue() {
	vector<int> v{ 4, 5, 2, 7, 9, 1 };
    sort(v.begin(), v.end(), Cmp());
    priority_queue<int, vector<int>, Cmp> qe;
	for (int val : v) {
		qe.push(val);
		//if (qe.size() > 3)
		//	qe.pop();
	}

	while (!qe.empty()) {
		cout << qe.top() << " ";
		qe.pop();
	}

	//for()
	cout << endl;
}

class Cmp1 {
public:
    bool operator()(const pair<string, int>& p1, const pair<string, int>& p2) {
        return p1.second == p2.second ? p1.first < p2.first : p1.second > p2.second;
        //return p1.second > p2.second;
        //return p1.first < p2.first;
    }
};

class Solution {
public:
    vector<string> topKFrequent(vector<string>& words, int k) {
        unordered_map<string, int> ump;
        //��ϣͳ�ƴ���
        for (auto& str : words) {
            ump[str]++;
        }

        priority_queue<pair<string, int>, vector<pair<string, int>>, Cmp1> heap;

        for (auto& it : ump) {
            heap.push(it);
            if (heap.size() > k)
                heap.pop();
        }

        vector<string> retV(k);

        for (int i = k - 1; i >= 0; i--) {
            retV[i] = heap.top().first;
            heap.pop();
        }
        return retV;
    }
};

void test() {
    Solution slu;
    vector<string> vs{ "i", "love", "leetcode", "i", "love", "coding" };
    vector<string> ret = slu.topKFrequent(vs, 4);
    for (auto& str : ret) {
        cout << str << " ";
    }
    cout << endl;
}
int main() {
	test_priority_queue();
    //test();
    return 0;
}
