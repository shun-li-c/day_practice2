#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<map>
#include<set>
#include<unordered_set>
#include<unordered_map>
using namespace std;

//-------------------------------set---------------------------/
//K模型
//排序 + 去重
void test_set() {
	set<int> st{ 4, 7, 8, 2, 1};
	auto it = st.begin();
	while (it != st.end()) {
		cout << *it << " ";
		it++;
	}
	cout << endl;

	//删除某个节点迭代器时,先查找后删除(必须要做判断)
	set<int>::iterator pos = st.find(7);
	//set<int>::iterator pos = st.find(10); //-->不判断直接删除会报错
	if (pos != st.end()) {
		//*pos = 10; -->不允许被修改
		st.erase(pos);
	}
	cout << st.erase(2) << endl;
	
	for (auto s : st) {
		cout << s << " ";
	}
	cout << endl;

	//----------------multiSet--------------/
	//multiple多个的
	//只排序不去重
	multiset<int> mst{ 1, 1, 3, 8, 3, 7, 0, };
	for (auto& ms : mst) {
		cout << ms << " ";
	}
	cout <<  endl;
	//查找值有多个时,返回中序第一个val值所在迭代器
	multiset<int>::iterator pos1 = mst.find(3);
	while (pos1 != mst.end() && *pos1 == 3) {
		cout << *pos1 << " ";
		++pos1;
	}
	cout << endl;
	//删除值时返回节点个数
	cout << mst.erase(3) << endl;

	//----------------------unordered_set--------------------/
	//unordered无序的
	unordered_set<int> ust{ 4, 7, 8, 2, 1, 1};
	for (auto& ut : ust) {
		cout << ut << " ";
	}
	cout << endl;

}
//--------------------------------map--------------------------------/
//K, v模型
//pair键值对-->实际就是一个类有两个成员first_type、second_type
//make_pair 构造一个pair
void test_map() {
	map<string, string> dict;
	dict.insert(make_pair("sort", "排序"));
	dict.insert(pair<string, string>("bubble", "冒泡"));
	dict.insert({"red", "红色"}); //C++11
	for (auto& dt : dict) {
		cout << dt.first << " " << dt.second << endl;
	}
	map<string, string>::iterator it = dict.begin();
	while (it != dict.end()) {
		//it->first = "black";
		//it->second = "黑色";   //-->第一个值不能被修改,第二个值可以被修改
		cout << it->first << " " << it->second << endl;
		it++;
	}
	map<char, int> mp;
	char buffer[] = { 'a', 'b', 'a', 'a', 'a', 'd', 'b' };
	//方法一
	for (auto& b : buffer) {
		auto sign = mp.insert(make_pair(b, 1));
		if (sign.second == false) {
			sign.first->second++;
		}
	}
	for (auto& m : mp) {
		cout << m.first << " " << m.second << endl;
	}
	mp.clear();
	//方法二
	//operator[]底层 -->返回的是value的值
	//mapped_type& operator[](const key_type& k)
	//{
	//	pair<iterator, bool> ret = insert(make_pair(k, mapped_type()));
	//	return ret.first->second;
	//}
	for (auto& b : buffer) {
		mp[b]++;
	}
	for (auto& m : mp) {
		cout << m.first << " " << m.second << endl;
	}
	//----------------------multimap--------------------/
	//可以有重复值不会合并
	multimap<string, string> mlt;
	mlt.insert(make_pair("sort", "排序"));
	mlt.insert(make_pair("bubble", "maopao"));
	mlt.insert(make_pair("left", "左"));
	mlt.insert(make_pair("left", "左"));
	mlt.insert(make_pair("left", "左"));
	mlt.insert(make_pair("left", "左"));

	//count 计数
	cout << mlt.count("left") << endl;
	for (auto& mt : mlt) {
		cout << mt.first << " " << mt.second << endl;
	}

	//--------------------unordered_map----------------/
	//不进行排序
	unordered_map<char, int> ump;
	for (auto& bf : buffer) {
		ump[bf]++;
	}
	for (auto& up : ump) {
		cout << up.first << " " << up.second << endl;
	}
}
int main()
{
	test_set();
	//test_map();
	return 0;
}

