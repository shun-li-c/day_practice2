#define _CRT_SECURE_NO_WARNINGS 1
#include"PageCache.h"

PageCache PageCache::_sInst;

//获取n页SpanNode
SpanNode* PageCache::NewSpan(size_t n) {
	assert(n > 0);
	//大于128页直接向堆申请内存
	if (n > NPAGES - 1) {
		void* ptr = SystemAlloc(n);
		
		//SpanNode* spanNode = new SpanNode;
		SpanNode* spanNode = _SpanNodepool.New();
		
		spanNode->_pageId = (PAGE_ID)ptr >> PAGE_SHIFT;
		spanNode->_n = n;

		//_idSpanNodeMap[spanNode->_pageId] = spanNode; //建立映射方便释放内存
		_idSpanNodeMap.set(spanNode->_pageId, spanNode);
		return spanNode;
	}

	//检查对应桶中是否有sapnNode
	if (!_pageLists[n].Empty()) {
		SpanNode* nSpanNode = _pageLists[n].PopFront();
		//将其中节点建立映射关系映射
		for (PAGE_ID i = 0; i < nSpanNode->_n; i++) {
			//_idSpanNodeMap[nSpanNode->_pageId + i] = nSpanNode;
			_idSpanNodeMap.set(nSpanNode->_pageId + i, nSpanNode);

		}
		return nSpanNode;
	}

	//检查后面桶中是否有SpanNode节点，有则进行切分
	for (size_t i = n + 1; i < NPAGES; i++) {
		if (!_pageLists[i].Empty()) {
			//找到第i个桶不为空时先将其取出，切分出n个后在放回相应桶中
			SpanNode* ISpanNode = _pageLists[i].PopFront();
			//开辟出一个节点进行切分,然后返回
			
			//SpanNode* NSpanNode = new SpanNode;
			SpanNode* NSpanNode = _SpanNodepool.New();

			NSpanNode->_n = n;
			NSpanNode->_pageId = ISpanNode->_pageId;

			ISpanNode->_pageId += n;
			ISpanNode->_n -= n;

			_pageLists[ISpanNode->_n].PushFront(ISpanNode);

			//存储ISpanNode起始页号映射方便回收
			//_idSpanNodeMap[ISpanNode->_pageId] = ISpanNode;
			//_idSpanNodeMap[ISpanNode->_pageId + ISpanNode->_n - 1] = ISpanNode;
			_idSpanNodeMap.set(ISpanNode->_pageId, ISpanNode);
			_idSpanNodeMap.set(ISpanNode->_pageId + ISpanNode->_n - 1, ISpanNode);

			for (PAGE_ID i = 0; i < NSpanNode->_n; i++) {
				//_idSpanNodeMap[NSpanNode->_pageId + i] = NSpanNode;
				_idSpanNodeMap.set(NSpanNode->_pageId + i, NSpanNode);
			}

			return NSpanNode;
		}
	}

	//走到这里说明后面的桶中没有剩余的SpanNode节点,找系统申请一页
	//SpanNode* bigSpanNode = new SpanNode;
	SpanNode* bigSpanNode = _SpanNodepool.New();

	void* ptr = SystemAlloc(NPAGES - 1);
	bigSpanNode->_pageId = (PAGE_ID)ptr >> PAGE_SHIFT; //右移13位相当于除8k
	bigSpanNode->_n = NPAGES - 1;

	_pageLists[bigSpanNode->_n].PushFront(bigSpanNode); //将申请的大页span放入哈希桶中

	return NewSpan(n); //重新调用进行切分

}
//获取从对象到SpanNode的映射
SpanNode* PageCache::MapObjectToSpanNode(void* obj) {
	PAGE_ID id = (PAGE_ID)obj >> PAGE_SHIFT;

	//std::unique_lock<std::mutex> lock(PageCache::GetInstance()->GetMutex()); //出作用域自动解锁
	//auto ret = _idSpanNodeMap.find(id);
	//if (ret != _idSpanNodeMap.end()) {
	//	//找到了进行返回
	//	return ret->second;
	//}
	//else {
	//	//找不到说明出问题了直接报错
	//	assert(false); 
	//	return nullptr;
	//}

	auto ret = (SpanNode*)_idSpanNodeMap.get(id);
	assert(ret != nullptr);
	return ret;
}

//释放空闲SpanNode回到PageCache，并合并相邻的SpanNode
void PageCache::ReleaseSpanNodeToPageCache(SpanNode* spanNode) {

	//大于128页直接还给堆
	if (spanNode->_n > NPAGES - 1) {
		//根据页号算出响应的地址,然后进行释放
		void* ptr = (void*)(spanNode->_pageId << PAGE_SHIFT);
		SystemFree(ptr);
		return;
	}

	//向前合并
	while (1) {
		PAGE_ID prevId = spanNode->_pageId - 1;
		//找不到跳出循环
		//auto ret = _idSpanNodeMap.find(prevId);
		//if (ret == _idSpanNodeMap.end()) {
		//	break;
		//}
		auto ret = (SpanNode*)_idSpanNodeMap.get(prevId);
		if (ret == nullptr) {
			break;
		}

		//prevSpanNode正在被使用跳出循环
		//SpanNode* prevSpanNode = ret->second;
		SpanNode* prevSpanNode = ret;
		if (prevSpanNode->_isUse == true) {
			break;
		}
		//合并出超出128kb的spanNode不进行管理
		if (prevSpanNode->_n + spanNode->_n > NPAGES - 1) {
			break;
		}

		//进行合并
		spanNode->_pageId = prevSpanNode->_pageId;
		spanNode->_n += prevSpanNode->_n;

		_pageLists[prevSpanNode->_n].Erase(prevSpanNode);

		//delete prevSpanNode;
		_SpanNodepool.Delete(prevSpanNode);
	}

	//向后合并
	while (1) {
		PAGE_ID nextId = spanNode->_pageId + spanNode->_n;
		//找不到跳出循环
		//auto ret = _idSpanNodeMap.find(nextId);
		//if (ret == _idSpanNodeMap.end()) {
		//	break;
		//}
		auto ret = (SpanNode*)_idSpanNodeMap.get(nextId);
		if (ret == nullptr) {
			break;
		}

		//prevSpanNode正在被使用跳出循环
		//SpanNode* nextSpanNode = ret->second;
		SpanNode* nextSpanNode = ret;
		if (nextSpanNode->_isUse == true) {
			break;
		}

		//合并出超出128kb的spanNode不进行管理
		if (nextSpanNode->_n + spanNode->_n > NPAGES - 1) {
			break;
		}

		//进行合并
		spanNode->_n += nextSpanNode->_n;
		_pageLists[nextSpanNode->_n].Erase(nextSpanNode);
		
		//delete nextSpanNode;
		_SpanNodepool.Delete(nextSpanNode);
	}

	//放回哈希桶中
	_pageLists[spanNode->_n].PushFront(spanNode);
	spanNode->_isUse = false;

	//存储ISpanNode起始页号映射方便回收
	//_idSpanNodeMap[spanNode->_pageId] = spanNode;
	//_idSpanNodeMap[spanNode->_pageId + spanNode->_n - 1] = spanNode;
	_idSpanNodeMap.set(spanNode->_pageId, spanNode);
	_idSpanNodeMap.set(spanNode->_pageId + spanNode->_n - 1, spanNode);
}

