#pragma once
#include"public.h"
#include"FixedMemoryPool.h"
#include"PageMap.h"
class PageCache {
private:
	SpanList _pageLists[NPAGES];
	//std::unordered_map<PAGE_ID, SpanNode*> _idSpanNodeMap;
	TCMalloc_PageMap1<32 - PAGE_SHIFT> _idSpanNodeMap;

	FiedMemoryPool<SpanNode> _SpanNodepool; //申请SpanNode节点的内存池
	std::mutex _PageMtx;
private:
	PageCache() {};
	PageCache(const PageCache&) = delete;
	static PageCache _sInst;
public:
	static PageCache* GetInstance() {
		return &_sInst;
	}

	std::mutex& GetMutex() {
		return _PageMtx;
	}
	//获取n页SpanNode
	SpanNode* NewSpan(size_t n);

	//获取从对象到SpanNode的映射
	SpanNode* MapObjectToSpanNode(void* obj);

	//释放空闲SpanNode回到PageCache，并合并相邻的SpanNode
	void ReleaseSpanNodeToPageCache(SpanNode* spanNode);

};


