#pragma once
#include"public.h"
#include"ThreadCache.h"
#include"PageCache.h"
static void* ConcurrentAlloc(size_t Byte_Size) {
	if (Byte_Size > MAX_BYTES) { //超过256kb
		size_t alignSize = SizeClass::AlignSize(Byte_Size); //对齐大小
		size_t nPage = alignSize >> PAGE_SHIFT;

		//直接找PageCache申请，PageCache会去找堆申请
		PageCache::GetInstance()->GetMutex().lock();
		SpanNode* SpanNode = PageCache::GetInstance()->NewSpan(nPage);
		SpanNode->_size = Byte_Size;
		PageCache::GetInstance()->GetMutex().unlock();

		void* ptr = (void*)(SpanNode->_pageId << PAGE_SHIFT);
		return ptr;
	}
	else {
		//找thread cache申请内存
		if (pTLSThreadCache == nullptr) {
			pTLSThreadCache = new ThreadCache;
		}
		//cout << std::this_thread::get_id() << " : " << pTLSThreadCache << endl;
		return pTLSThreadCache->Allocate(Byte_Size);
	}
}

static void ConcurrentFree(void* ptr) {
	SpanNode* spanNode = PageCache::GetInstance()->MapObjectToSpanNode(ptr); //获取SpanNode对象
	size_t byte_size = spanNode->_size;
	if (byte_size > MAX_BYTES) {
		//比256字节还大直接还给堆
		PageCache::GetInstance()->GetMutex().lock();
		PageCache::GetInstance()->ReleaseSpanNodeToPageCache(spanNode);
		PageCache::GetInstance()->GetMutex().unlock();
	}
	else {
		assert(pTLSThreadCache);
		pTLSThreadCache->Deallocate(ptr, byte_size);
	}
}