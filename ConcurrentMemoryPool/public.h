#pragma once
#include<iostream>
#include<unordered_map>
#include<assert.h>
#include<thread>
#include<mutex>
#include<algorithm>
#include<time.h>


using std::cout;
using std::endl;

static const size_t PAGE_SHIFT = 13;
static const size_t MAX_BYTES = 256 * 1024; //最大字节数
static const size_t NFREELIST = 208;
static const size_t NPAGES = 129; //下标是从0开始的

#ifdef _WIN32
#include <windows.h>
#else
// ...
#endif

#ifdef _WIN64
typedef unsigned long long PAGE_ID;
#elif _WIN32
typedef size_t PAGE_ID;
#else
//linux环境

#endif

// 去堆上按页申请空间
inline static void* SystemAlloc(size_t kpage)
{
#ifdef _WIN32
	void* ptr = VirtualAlloc(0, kpage << 13, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
#else
	// linux下brk mmap等
#endif

	if (ptr == nullptr)
		throw std::bad_alloc();

	return ptr;
}

inline static void SystemFree(void* ptr)
{
#ifdef _WIN32
	VirtualFree(ptr, 0, MEM_RELEASE);
#else
	// linux下sbrk unmmap等
#endif
}


//取头4/8字节 --> 下一个节点地址
static void*& CurNext(void* obj) {
	return *(void**)obj;
}
class FreeList {
private:
	void* _freelist = nullptr;
	size_t _size = 0;    //记录链表长度
	size_t _MaxSize = 1; //控制慢增长
public:
	void push(void* obj) {
		assert(obj);
		//头插
		CurNext(obj) = _freelist;
		_freelist = obj;
		++_size;
	}
	void PushRange(void* start, void* end, size_t n) {
		//1、条件断点
		//2、查看栈帧
		//调试检查链实际数量是否和给过来的一样
		//int i = 0;
		//void* cur = start;
		//while (cur) {
		//	i++;
		//	cur = CurNext(cur);
		//}
		//if (i != n) {
		//	int a = 0;
		//}
		CurNext(end) = _freelist;
		_freelist = start;
		_size += n;
	}
	void* popFront() {
		assert(_freelist);
		void* cur = _freelist;
		_freelist = CurNext(_freelist);
		--_size;
		return cur;
	}
	void PopRange(void*& start, void*& end, size_t n) {
		if (n > _size) {
			int a = 0;
		}
		assert(n <= _size);

		start = _freelist;
		end = start;
		for (int i = 0; i < n - 1; i++) {
			end = CurNext(end);
		}
		_freelist = CurNext(end);
		CurNext(end) = nullptr;
		_size -= n;
	}
	bool Empty() {
		return _freelist == nullptr;
	}
	size_t& Byte_Size() {
		return _size;
	}
	size_t& MaxSize() {
		return _MaxSize;
	}
};

//计算对象大小对齐规则
class SizeClass {
public:
	// 整体控制在最多10%左右的内碎片浪费
	// [1,128]					8byte对齐	    freelist[0,16)
	// [128+1,1024]				16byte对齐	    freelist[16,72)
	// [1024+1,8*1024]			128byte对齐	    freelist[72,128)
	// [8*1024+1,64*1024]		1024byte对齐     freelist[128,184)
	// [64*1024+1,256*1024]		8*1024byte对齐   freelist[184,208)
	
	//20 8 --> 24
	//110 8 --> 112
	//容易想到的
	//size_t _AlignSize(size_t bytes, size_t alignNum) {
	//	size_t alignSize;
	//	if (bytes % alignNum == 0) {
	//		alignSize = bytes;
	//	}
	//	else {
	//		alignSize = (bytes / alignNum + 1) * alignNum;
	//	}
	//  return alignSize;
	//}

	//好的实现方法
	static inline size_t _AlignSize(size_t bytes, size_t alignNum) {
		return ((bytes + alignNum - 1) & ~(alignNum - 1));
	}
	//对齐大小计算
	static inline size_t AlignSize(size_t bytes) {
		//assert(bytes <= MAX_BYTES);

		if (bytes <= 128) {
			return _AlignSize(bytes, 8);
		}
		else if (bytes <= 1024) {
			return _AlignSize(bytes, 16);
		}
		else if (bytes <= 8 * 1024) {
			return _AlignSize(bytes, 128);
		}
		else if (bytes <= 64 * 1024) {
			return _AlignSize(bytes, 1024);
		}
		else if (bytes <= 256 * 1024) {
			return _AlignSize(bytes, 8 * 1024);
		}
		else {
			return _AlignSize(bytes, 1 << PAGE_SHIFT); //页对齐
		}
	}

	//容易想到的
	//size_t _Index(size_t bytes, size_t alignNum) {
	//	if (bytes % alignNum == 0) {
	//		return bytes / alignNum - 1; //下标从0开始
	//	}
	//	else {
	//		return bytes / alignNum;
	//	}
	//}

	//20 3  --> 1
	//130 4 --> 8
	//好的实现方法
	static inline size_t _Index(size_t bytes, size_t align_shift) {
		return ((bytes + (1 << align_shift) - 1) >> align_shift) - 1;
	}

	// 计算映射的哪一个自由链表桶
	static inline size_t Index(size_t bytes) {
		assert(bytes <= MAX_BYTES);

		static int group_array[4] = { 16, 56, 56, 56 }; // 每个区间有多少个链
		if (bytes <= 128) {
			return _Index(bytes, 3); //传2的次方
		}
		else if (bytes <= 1024) {
			return _Index(bytes - 128, 4) + group_array[0];
		}
		else if (bytes <= 8 * 1024) {
			return _Index(bytes - 1024, 7) + group_array[1] + group_array[0];
		}
		else if (bytes <= 64 * 1024) {
			return _Index(bytes - 8 * 1024, 10) + group_array[2] + group_array[1] + group_array[0];
		}
		else if (bytes <= 256 * 1024) {
			return _Index(bytes - 64 * 1024, 13) + group_array[3] + group_array[2] + group_array[1] + group_array[0];
		}
		else {
			assert(false);
		}

		return -1;
	}

	// 一次thread cache从中心缓存获取多少个
	//也就是计算可以给到你几个对象
	//其中上限512,下限2也可以理解为限制桶中链表的长度
	static size_t NumMoveSize(size_t Byte_Size)
	{
		assert(Byte_Size > 0);

		// [2, 512]，一次批量移动多少个对象的(慢启动)上限值
		//对象越小，计算出的上限越高
		//对象越大，计算出的上限越低
		int num = MAX_BYTES / Byte_Size;
		if (num < 2)
			num = 2;

		if (num > 512)
			num = 512;

		return num;
	}

	// 计算一次向系统获取几个页
	// 单个对象 8byte
	// ...
	// 单个对象 256KB
	static size_t NumMovePage(size_t Byte_Size)
	{
		size_t num = NumMoveSize(Byte_Size);
		size_t npage = num * Byte_Size;

		npage >>= PAGE_SHIFT;
		if (npage == 0)
			npage = 1;

		return npage;
	}
};

struct SpanNode {
	PAGE_ID _pageId = 0; //大块内存起始页号
	size_t _n = 0; //页的数量

	SpanNode* _next = nullptr;
	SpanNode* _prev = nullptr;

	void* _freeList = nullptr; //自由链表
	size_t _size = 0; //切好小对象大小
	size_t _useCount = 0; //分配给thread cache小块内存数量
	
	bool _isUse = false; //是否正在被使用

};

class SpanList {
private:
	SpanNode* _head = nullptr; //头节点
	std::mutex _mtx; //桶锁
public:
	SpanList() {
		_head = new SpanNode;
		_head->_next = _head;
		_head->_prev = _head;
	}
	~SpanList() {
		delete _head;
	}
	std::mutex& getMutex() {
		return _mtx;
	}
	SpanNode* Begin() {
		return _head->_next;
	}
	SpanNode* end() {
		return _head;
	}
	bool Empty() {
		return _head->_next == _head;
	}
	void Insert(SpanNode* pos, SpanNode* newSpan) {
		assert(pos && newSpan);
		SpanNode* prev = pos->_prev;

		prev->_next = newSpan;
		newSpan->_prev = prev;

		pos->_prev = newSpan;
		newSpan->_next = pos;	

	}

	void Erase(SpanNode* pos) {
		assert(pos);
		//1、条件断点
		//2、查看栈帧
		//if (pos == _head) {
		//	int a = 0;
		//}
		assert(pos != _head);

		SpanNode* prev = pos->_prev;
		SpanNode* next = pos->_next;

		prev->_next = next;
		next->_prev = prev;
	}

	SpanNode* PopFront() {
		SpanNode* ret = _head->_next;
		Erase(ret);
		return ret;
	}

	void PushFront(SpanNode* spanNode) {
		Insert(Begin(), spanNode);
	}
};