#pragma once
#include"public.h"

//单例模式（懒汉）
class CentralCache {
private:
	SpanList _SpanLists[NFREELIST];
private:
	CentralCache() {}
	CentralCache(const CentralCache&) = delete;
	static CentralCache _sInst; //类外初始化
public:
	static CentralCache* GetInStance() {
		return &_sInst;
	}

	// 获取一个非空的spanNode
	SpanNode* GetOneSpan(SpanList& list, size_t Byte_Size);

	// 从中心缓存获取一定数量的对象给thread cache
	size_t FetchRangeObj(void*& start, void*& end, size_t batchNum, size_t Byte_Size);

	// 将一定数量的对象释放到span
	void ReleaseListToSpans(void* start, size_t byte_size);
};