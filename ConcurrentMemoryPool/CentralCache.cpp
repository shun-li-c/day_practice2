#define _CRT_SECURE_NO_WARNINGS 1
#include"public.h"
#include"CentralCache.h"
#include "PageCache.h"

CentralCache CentralCache::_sInst;
// 获取一个非空的spanNode
SpanNode* CentralCache::GetOneSpan(SpanList& list, size_t Byte_Size) {
	//查看当前桶中的每个SpanNode节点是否有未分配的对象
	SpanNode* it = list.Begin();
	while (it != list.end()) {
		if (it->_freeList != nullptr) {
			return it;
		}
		else {
			it = it->_next;
		}
	}
	//走到这里说明当前桶中每个节点没有未分配的对象了,找pageCache要
	
	//先把桶锁解掉，这样在其他线程释放对象时不会阻塞
	list.getMutex().unlock();
	PageCache::GetInstance()->GetMutex().lock();
	SpanNode* spanNode = PageCache::GetInstance()->NewSpan(SizeClass::NumMovePage(Byte_Size));
	spanNode->_isUse = true;
	spanNode->_size = Byte_Size; //更新大小
	PageCache::GetInstance()->GetMutex().unlock();

	//对获取的span进行切分,不需要加锁因为当前申请内存未放入链表中其他线程不会访问到这个span
	//计算span大块内存的起始地址和大块内存的大小(字节数)
	char* start = (char*)(spanNode->_pageId << PAGE_SHIFT);
	size_t bytes = spanNode->_n << PAGE_SHIFT;
	char* end = start + bytes;

	//把大块内存切成自由链表链接起来(先切一小块做头然后进行尾插)
	spanNode->_freeList = start;
	start += Byte_Size;
	void* tail = spanNode->_freeList;
	int i = 1;
	while (start < end) {
		++i;
		CurNext(tail) = start;
		tail = CurNext(tail);
		start += Byte_Size;
	}
	CurNext(tail) = nullptr;
	//切好后,把span挂到桶中去,先进行加锁
	list.getMutex().lock();
	list.PushFront(spanNode);
	return spanNode;
	
}

// 从中心缓存获取一定数量的对象给thread cache
size_t CentralCache::FetchRangeObj(void*& start, void*& end, size_t batchNum, size_t Byte_Size) {
	size_t index = SizeClass::Index(Byte_Size);

	//这里会涉及多个线程同时从中心缓存获取内存的情况，故应该加锁
	_SpanLists[index].getMutex().lock();
	SpanNode* spanNode = GetOneSpan(_SpanLists[index], Byte_Size);
	assert(spanNode);
	assert(spanNode->_freeList);

	//从spanNode中获取batchNum个对象,如果不够有多少拿多少
	start = spanNode->_freeList;
	end = start;
	int i = 0;
	int ActualNum = 1;
	while (i < batchNum - 1 && CurNext(end) != nullptr) {
		end = CurNext(end);
		++i;
		++ActualNum;
	}
	//从spanNode将这段链表删除,并统计出该spanNode中自由链表已经使用的数量
	spanNode->_freeList = CurNext(end);
	CurNext(end) = nullptr;
	spanNode->_useCount += ActualNum;
	_SpanLists[index].getMutex().unlock();

	return ActualNum;
}

//将一定数量的对象释放到span
void CentralCache::ReleaseListToSpans(void* start, size_t byte_size) {
	size_t index = SizeClass::Index(byte_size);

	_SpanLists[index].getMutex().lock();
	//将传过来的链表一个个头插进对应的spanNode中
	while (start) {
		void* next = CurNext(start);
		//通过链表每个节点的地址来获取对应的spanNode地址
		SpanNode* spanNode = PageCache::GetInstance()->MapObjectToSpanNode(start);
		CurNext(start) = spanNode->_freeList;
		spanNode->_freeList = start;
		spanNode->_useCount--; //每头插回来一个已使用数量减一

		//当SpanNode已使用数量为0时说明该节点中的自由链表节点都还回来了,继续归还给PageCache处理
		if (spanNode->_useCount == 0) {
			//归还给PageCache

			//先将对应桶中的SpanNode节点删除
			_SpanLists[index].Erase(spanNode);
			spanNode->_prev = nullptr;
			spanNode->_next = nullptr;
			spanNode->_freeList = nullptr;

			//释放span给page cache时，使用pagecache的锁就可以了
			//这时把桶锁解掉
			_SpanLists[index].getMutex().unlock();
			PageCache::GetInstance()->GetMutex().lock();
			PageCache::GetInstance()->ReleaseSpanNodeToPageCache(spanNode);
			PageCache::GetInstance()->GetMutex().unlock();

			_SpanLists[index].getMutex().lock();
		}

		start = next;
	}
	_SpanLists[index].getMutex().unlock();
}

