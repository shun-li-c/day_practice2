#pragma once
#include"public.h"

class ThreadCache {
private:
	FreeList _freelists[NFREELIST];
public:
	void* Allocate(size_t Byte_Size);  //申请内存
	void Deallocate(void* ptr, size_t Byte_Size); //释放内存
	void ListTooLong(FreeList& list, size_t Byte_Size); //链表太长释放链表

	//从中心缓存获取对象
	void* FetchFromCentralCache(size_t index, size_t Byte_Size);
};

//TLS thread local storage  --> 线程局部存储
static _declspec(thread) ThreadCache* pTLSThreadCache = nullptr;