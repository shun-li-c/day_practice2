#define _CRT_SECURE_NO_WARNINGS 1
#include"ThreadCache.h"
#include"CentralCache.h"
//申请内存
void* ThreadCache::Allocate(size_t Byte_Size) {
	assert(Byte_Size <= MAX_BYTES);
	//传过来申请字节数计算出对齐大小(实际给到的内存大小)
	size_t alignSize = SizeClass::AlignSize(Byte_Size);
	size_t index = SizeClass::Index(Byte_Size);
	if (!_freelists[index].Empty()) {
		return _freelists[index].popFront();
	}
	else {
		//从中心缓存获取对象
		return FetchFromCentralCache(index, alignSize);
	}
}
//释放内存
void ThreadCache::Deallocate(void* ptr, size_t Byte_Size) {
	assert(ptr);
	assert(Byte_Size <= MAX_BYTES);
	//找到对应桶位置进行插入
	size_t index = SizeClass::Index(Byte_Size);
	_freelists[index].push(ptr);

	//当链表长度大于一次申请的最大内存值将其还给central cache
	if (_freelists[index].Byte_Size() >= _freelists[index].MaxSize()) {
		ListTooLong(_freelists[index], Byte_Size);
	}
}
//链表太长释放链表
void ThreadCache::ListTooLong(FreeList& list, size_t Byte_Size) {
	//首先从原链表中将这段链表删除,接着还给中心缓存
	//在freelist中增加区间删除函数
	void* start = nullptr;
	void* end = nullptr;
	list.PopRange(start, end, list.Byte_Size());

	//将链表还给中心缓存
	CentralCache::GetInStance()->ReleaseListToSpans(start, Byte_Size);
}

//从中心缓存获取对象
void* ThreadCache::FetchFromCentralCache(size_t index, size_t Byte_Size) {
	//慢反馈调节算法
	//1、最开始不会一次向central cache批量要太多，因为要太多有可能会用不完
	//2、如果不断Byte_Size大小内存需求，那么batchNum就会不断增长直到上限
	//3、Byte_Size越小一次向central cache要的batchNum越小
	//4、Byte_Size越大一次向central cache要的batchNum越大
	size_t batchNum = min(_freelists[index].MaxSize(), SizeClass::NumMoveSize(Byte_Size));
	if (_freelists[index].MaxSize() == batchNum) {
		_freelists[index].MaxSize() += 1;
	}

	//调用cnetral cache中获取对象接口
	void* start = nullptr;
	void* end = nullptr;
	size_t ActualNum = CentralCache::GetInStance()->FetchRangeObj(start, end, batchNum, Byte_Size);
	assert(ActualNum > 0); //最少获取一个

	if (ActualNum == 1) {
		assert(start == end);
		return start;
	}
	else {
		//获取到一个批量
		_freelists[index].PushRange(CurNext(start), end, ActualNum - 1);
		return start;
	}
}
