#pragma once
#include"public.h"

template<class T>
class FiedMemoryPool {
private:
	char* _memory = nullptr;   //指向大块内存的指针
	void* _freeList = nullptr;//还内存链接自由链表头指针
	size_t _residueBytes = 0;  //剩余字节数
public:
	T* New() {
		T* obj = nullptr;
		//优先把还回来的对象重复利用
		if (_freeList) {
			void* next = *((void**)_freeList);
			obj = (T*)_freeList;
			_freeList = next;
		}
		else {
			//剩余内存不够一个对象大小时重新开辟内存
			if (_residueBytes < sizeof(T)) {
				_residueBytes = 128 * 1024;
				_memory = (char*)SystemAlloc(_residueBytes >> PAGE_SHIFT);
				if (_memory == nullptr) {
					throw std::bad_alloc();
				}
			}
			//给目标分配内存
			obj = (T*)_memory;
			size_t objSize = sizeof(T) < sizeof(void*) ? sizeof(void*) : sizeof(T); //至少开辟一个指针大小
			_memory += objSize;
			_residueBytes -= objSize;
		}
		//用定位new显示调用其构造函数
		new (obj)T;

		return obj;
	}
	void Delete(T* obj) {
		//显示调用其析构函数
		obj->~T();

		//头插进自由链表
		*(void**)obj = _freeList;
		_freeList = obj;
	}
};