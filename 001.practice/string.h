#pragma once
#include<assert.h>
#include<iostream>
using namespace std;
namespace zhu {
	class string {
	private:
		char* _str;
		size_t _size;
		size_t _capacity;
		static const size_t npos;
	public:
		typedef char* iterator;
		typedef const char* const_iterator;
		string(const char* str = "")
			:_size(strlen(str))
			,_capacity(_size)
		{
			_str = new char[_capacity + 1];
			strcpy(_str, str);
		}

		~string() {
			delete[] _str;
			_str = nullptr;
			_size = _capacity = 0;
		}
		//��ͳд��
		//string(const string& s) 
		//	:_size(s._size)
		//	,_capacity(s._capacity)
		//{
		//	_str = new char[_capacity + 1];
		//	strcpy(_str, s._str);
		//}
		////��ֵ���� 
		//string& operator=(const string& s){
		//	if (this != &s) {
		//		char* temp = new char[s._capacity + 1];
		//		strcpy(temp, s.c_str());
		//		delete[] _str;
		//		_str = temp;
		//		_size = s._size;
		//		_capacity = s._capacity;
		//	}
		//	return *this;
		//}
		
		//�ִ�д��
		string(const string& s) 
			:_str(nullptr)
		{
			string temp(s.c_str());
			_size = s._size;
			_capacity = s._capacity;
			std::swap(_str, temp._str);
		}
		string& operator=(string s) {
			std::swap(_str, s._str);
			_size = s._size;
			_capacity = s._capacity;
			return *this;
		}
		//����
		void reserve(size_t n) {
			if (n > capacity()) {
				char* temp = new char[n + 1];
				strcpy(temp, _str);
				delete _str;
				_str = temp;
				_capacity = n;
			}
		}
		//���ݲ���ʼ��
		void resize(size_t n, char ch = '\0') {
			if (n < size()) {
				_str[n] = '\0';
				_size = n;
			}
			else {
				if (n > _capacity) {
					reserve(n);
				}
				memset(_str + _size, ch, n - _size);
				_size = n;
				_str[_size] = '\0';
			}
		}
		void push_back(char ch) {
			if (_size == _capacity) {
				reserve(capacity() == 0 ? 4 : capacity() * 2);
			}
			_str[_size++] = ch;
			_str[_size] = '\0';

		}
		//����һ���ַ�
		string& insert(size_t pos, char ch) {
			assert(pos <= _size);
			if (_size == _capacity) {
				reserve(capacity() + 1);
			}
			int end = _size + 1;
			while (end > pos) {
				_str[end] = _str[end - 1];
				--end;
			}
			_str[pos] = ch;
			++_size;
			return *this;
		}
		//�����ַ���
		string& insert(size_t pos, const char* s) {
			assert(pos <= _size);
			int len = strlen(s);
			if (_size + len > capacity()) {
				reserve(_size + len + 1);
			}
			int end = _size + len;
			while (end >= pos + len) {
				_str[end] = _str[end - len];
				end--;
			}
			strncpy(_str + pos, s, len);
			_size += len;
			return *this;
		}
		//�����ַ���
		string& append(const string& s) {
			int len = strlen(s.c_str());
			if (_size + len > capacity()) {
				reserve(_size + len + 1);
			}
			strcpy(_str + _size, s.c_str());
			_size += len;
			return *this;
		}
		//�����ַ�
		size_t find(char c) {
			for (size_t i = 0; i < _size; i++) {
				if (_str[i] == c) {
					return i;
				}
			}
			return npos;
		}
		//�����ַ���
		size_t find(const char* s, size_t pos = 0) {
			const char* ptr = strstr(_str + pos, s);
			if (ptr == nullptr) {
				return npos;
			}
			else {
				return ptr - _str;
			}
		}
		string& erase(size_t pos = 0, size_t len = npos) {
			assert(pos <= _size);
			if (len == npos || pos + len >= size()) {
				_str[pos] = '\0';
				_size = pos;
			}
			else {
				//size_t n = _size - pos - len;
				//while (n > 0) {
				//	_str[pos] = _str[pos + len];
				//	pos++;
				//	n--;
				//}
				strcpy(_str + pos, _str + pos + len);
				_size -= len;
				_str[_size] = '\0';

			}
			return *this;
		}
		size_t capacity() {
			return _capacity;
		}
		size_t size() const {
			return _size;
		}
		void clean() {
			erase();
		}
		const char* c_str() const {
			return _str;
		}
		char& operator[](size_t pos){
			assert(pos < size());
			return _str[pos];
		}
		const char& operator[](size_t pos) const {
			assert(pos < size());
			return _str[pos];
		}
		iterator begin() {
			return _str;
		}
		iterator end() {
			return _str + _size;
		}
		const_iterator begin() const{
			return _str;
		}
		const_iterator end() const{
			return _str + _size;
		}
		string& operator+=(char c) {
			push_back(c);
			return *this;
		}
		string& operator+=(const char* s) {
			append(s);
			return *this;
		}

	};
	istream& operator>>(istream& in, string& s) {
		s.clean();
		char ch = in.get();
		while (ch != ' ' && ch != '\n') {
			s += ch;
			ch = in.get();
		}
		return in;
	}
	ostream& operator<<(ostream& out, const string& str) {
		for (size_t i = 0; i < str.size(); i++) {
			out << str[i];
		}
		return out;
	}
	bool operator<(const string& s1, const string& s2) {
		return strcmp(s1.c_str(), s2.c_str()) < 0;
	}
	bool operator>(const string& s1, const string& s2) {
		return strcmp(s1.c_str(), s2.c_str()) > 0;
	}
	bool operator==(const string& s1, const string& s2) {
		return strcmp(s1.c_str(), s2.c_str()) == 0;
	}
	bool operator>=(const string& s1, const string& s2) {
		return s1 > s2 || s1 == s2;
	}
	bool operator<=(const string& s1, const string& s2) {
		return s1 < s2 || s1 == s2;
	}	
	bool operator!=(const string& s1, const string& s2) {
		return !(s1 == s2);
	}

	const size_t string::npos = -1;
	void test_string() {
		string str;
		str.push_back('a');
		str.insert(1, 'b');
		str.insert(1, 'c');
		str.insert(0, "cvb");
		str.erase(1, 2);
		string str1(str);
		string str2;
		str2.insert(0, "dfghjhgg");

		str = str2;
		std::cout << (str == str2) << std::endl;
		string str3;
		cin >> str3;
		cout << str3 << endl;
	}
}