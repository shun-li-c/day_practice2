#pragma once
#include<iostream>
//简介string实现-->不考虑增删查改
namespace mini_string {
	class string {
	private:
		char* _str;
	public:
		string(const char* s = "") {
			_str = new char[10];
			strcpy(_str, s);
		}
		string(const string& s)
			:_str(nullptr) 
			//一定要加上,因为可能指向随机值,temp析构会引起崩溃问题
		{
			string temp(s._str);	
			std::swap(_str, temp._str);
		}
		string& operator=(string s) {
			std::swap(_str, s._str);
			return *this;
		}
		~string() {
			if (_str) {
				delete[] _str;
				_str = nullptr;
			}
		}
	};

	void test() {
		string str = "asdfg";
		string str1 = "fffff";
		string str2(str);
	}
}