#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<algorithm>
#include<thread>
using namespace std;

vector<int> tmp;
int minPrice(vector<int>& v, int m, int p, int bench, int count) {
    if (count == m) {
        if (bench == p) {
            sort(tmp.begin(), tmp.end());
            return 1;
        }
        return -1;
    }
    for (int i = 0; i < v.size(); i++) {
        tmp.push_back(v[i]);
        if (bench + v[i] * v[i] > p)
            break;
        bench = bench + v[i] * v[i];
        if (minPrice(v, m, p, bench, count + 1) == 1)
            return 1;
        bench = bench - v[i] * v[i];
        tmp.pop_back();
    }
    return -1;
}
void test() {
    int N, M, P;
    cin >> N >> M >> P;
    vector<int> v;
    //vector<int> nums(M + 1, 0);
    for (int i = 1; i <= N; i++) {
        v.push_back(i);
    }
    int ret = minPrice(v, M, P, 0, 0);
    if (ret == 1) {
        for (auto& avl : tmp)
            cout << avl << " ";
    }
}

void printf1() {
    cout << "thread_id:" << this_thread::get_id() << " printf1()" << endl;
}
void printf2() {
    cout << "thread_id:" << this_thread::get_id() << " printf2()" << endl;
}
void test_thread() {
    thread t1(printf1);
    //t1.detach();
    t1.join();
}
int main() {
    //test_thread();
    int* arr = new int[10];
    delete[] arr;
    return 0;
}
