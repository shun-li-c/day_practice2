#define _CRT_SECURE_NO_WARNINGS 1
//#include<iostream>
//
//int main() {
//	return 0;
//}

#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;

int main() {
    vector<vector<string>> maze(10, vector<string>(10, "0"));
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            cin >> maze[i][j];
        }
    }

    //由题意可知二维数组四条变均为墙壁
    vector<vector<string>> dp(maze);
    dp[0][1] = '0';
    for (int i = 1; i < 10; i++) { //入口直接在第一行操作,入口点在第一行
        for (int j = 1; j < 10; j++) {
            if (dp[i][j] == "#")
                continue;
            string tmp1 = "0";
            string tmp2 = "0";

            if (strcmp(dp[i - 1][j].c_str(), "#") != 0) {
                tmp1 = dp[i - 1][j];
            }
            if (dp[i][j - 1] != "#") {
                tmp2 = dp[i][j - 1];
            }

            dp[i][j] = to_string(max(atoi(tmp1.c_str()), atoi(tmp2.c_str())));
        }
    }
    cout << atoi(dp[9][8].c_str()) << endl;
    return 0;
}
