#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<stack>
#include<unordered_map>
using namespace std;


struct TreeNode {
	int val;
 	struct TreeNode *left;
 	struct TreeNode *right;
 	TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};


class Solution {
public:

    //方法一: 两次遍历
    // void __FindPath(TreeNode* root, int sum, int c_sum, int& count){
    //     if(root == nullptr)
    //         return;
    //     c_sum += root->val;

    //     if(c_sum == sum){
    //         count++;
    //     }

    //     __FindPath(root->left, sum, c_sum, count);
    //     __FindPath(root->right, sum, c_sum, count);

    // }
    // void _FindPath(TreeNode* root, int sum, int c_sum, int& count){
    //     if(root == nullptr)
    //         return;
    //     __FindPath(root, sum, c_sum, count);

    //     _FindPath(root->left, sum, c_sum, count);
    //     _FindPath(root->right, sum, c_sum, count);
    // }
    // int FindPath(TreeNode* root, int sum) {
    //     if(root == nullptr){
    //         return 0;
    //     }
    //     int count = 0;
    //     int c_sum = 0;

    //     _FindPath(root, sum, c_sum, count);
    //     return count;
    // }

    //方法二：借助哈希表，不管三七二十一先把到该节点和记录下来，记得回退因为其余路径用不到
    unordered_map<int, int> ump;
    int dfs(TreeNode* root, int sum, int bench) {
        if (root == nullptr)
            return 0;
        int ret = 0;
        bench += root->val; //该值为从根节点到该节点的路径和
        if (ump.find(bench - sum) != ump.end()) {
            ret += ump[bench - sum];  //能用到则加上去
        }

        ump[bench]++; //将到当前节点和记录下来
        ret += dfs(root->left, sum, bench);
        ret += dfs(root->right, sum, bench);
        ump[bench]--; //其他路径不需要,回退
        return ret;
    }
    int FindPath(TreeNode* root, int sum) {
        if (root == nullptr)
            return 0;
        ump[0] = 1;  //路径和为0时说明基准值和要求值相等，算作一次
        int bench = 0;
        return dfs(root, sum, bench);
    }
};

int main() {
	return 0;
}

