#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<unordered_map>
using namespace std;

class Solution {
public:
    static vector<int> dailyTemperatures(vector<int>& temperatures) {
        int n = temperatures.size();
        vector<int> retV(n, 0);
        unordered_map<int, int> ump; //记录下标和值的映射
        //从后向前用hash存储值
        //ump[n - 1] = temperatures[n - 1];
        for (int i = n - 1; i >= 0; i--) {
            ump[i] = temperatures[i];
            for (int j = i + 1; j < n; j++) {
                if (ump[j] > temperatures[i]) {
                    retV[i] = j - i;
                    break;
                }
            }
        }
        return retV;
    }
};

class Solution1 {
public:
    static int countSubstrings(string s) {
        //动归
        //dp[i][j]为从下标i到下标j位置字符串为回文子串
        int n = s.size();
        vector<vector<bool>> dp(n, vector<bool>(n, false));
        //初始化，单个字符为回文子串
        for (int i = 0; i < n; i++) {
            dp[i][i] = true;
        }
        for (int i = 1; i < n; i++) {
            for (int j = 0; j < i; j++) {
                //头尾相等且区间为2或3即为回文串
                if (s[i] == s[j] && (i - j == 1 || i - j == 2)) {
                    dp[i][j] = true;
                }
                if (s[i] == s[j] && dp[i - 1][j + 1] == true) {
                    dp[i][j] = true;
                }
            }
        }

        int count = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (dp[i][j] == true)
                    count++;
            }
        }
        return count;
    }
};

int main() {
    vector<int> arr{ 73,74,75,71,69,72,76,73 };
    Solution::dailyTemperatures(arr);

    string str = "babab";
    Solution1::countSubstrings(str);
    cout << str.substr(1, 3);
	return 0;
}
