#define _CRT_SECURE_NO_WARNINGS 1
#include<vector>
#include<iostream>
#include<algorithm>
using namespace std;

//--------------------------------钱币找零----------------------------/
//假设1元、2元、5元、10元、20元、50元、100元的纸币分别有c0, c1, c2, c3, c4, c5, c6张。
//现在要用这些钱来支付K元，至少要用多少张纸币？
//贪心策略:用贪心算法的思想，很显然，每一步尽可能用面值大的纸币即可。
struct cmp {
	bool operator()(const vector<int>& v1, const vector<int>& v2) {
		return v1[0] > v2[0];
	}
};
int maxNum(vector<vector<int>>& vv, int money) {
	sort(vv.begin(), vv.end(), cmp());
	int num = 0;
	int n = 0;
	for (auto& v : vv) {
		n = money / v[0];
		n = min(n, v[1]);
		money -= n * v[0];
		num += n;
	}
	if (money != 0)
		return -1;
	return num;
}
int test() {
	vector<vector<int>> vv{ {1, 10}, {2, 5}, {5, 5}, {10, 6}, {20, 4}, {50, 10}, {100, 1} };
	int money = 0;
	cin >> money;
	cout << maxNum(vv, money) << endl;
	return 0;
}
//-----------------------------------多机调度问题--------------------------/
//某工厂有n个独立的作业，由m台相同的机器进行加工处理。作业i所需的加工时间为ti，任何作业在被处理时不能中
//断，也不能进行拆分处理。现厂长请你给他写一个程序：算出n个作业由m台机器加工处理的最短时间
//输入第一行T（1 < T < 100)表示有T组测试数据。每组测试数据的第一行分别是整数n，m（1 <= n <= 10000，
//	1 <= m <= 100），接下来的一行是n个整数ti（1 <= t <= 100)。
//输出
//	所需的最短时间
bool cmp1(const int& x1, const int& x2) {
	return x1 > x2;
}
int greedStrategy(vector<int>& works, vector<int>& machines) {
	//排降序
	sort(works.begin(), works.end(), cmp1);
	
	int workNum = works.size();
	int machineNum = machines.size();
	//作业数小于机器数,直接返回最大作业时长
	if (workNum <= machineNum) {
		return works[0];
	}
	else {
		//为每个机器分配作业
		for (int i = 0; i < workNum; i++) {
			//选择作业时长最小的机器分配作业
			int minTime = 0;
			for (int j = 0; j < machineNum; j++) {
				if (machines[minTime] > machines[j]) {
					minTime = j;
				}
			}
			machines[minTime] += works[i];
		}
	}
	//从机器中找出执行时间最大值
	int MaxTime = machines[0];
	for (int i = 1; i < machineNum; i++) {
		if (MaxTime < machines[i])
			MaxTime = machines[i];
	}
	return MaxTime;
}
void test2() {
	int n = 0, m = 0;
	cout << "请输入作业数和机器数,以空格间隔" << endl;
	cin >> n >> m;
	vector<int> works(n);
	vector<int> machines(m, 0);
	cout << "请依次输入作业数时长,以空格间隔" << endl;
	for (int i = 0; i < n; i++) {
		cin >> works[i];
	}
	cout << greedStrategy(works, machines) << endl;
}

//-----------------------------活动选择-------------------------------/
//有n个需要在同一天使用同一个教室的活动a1, a2, …, an，教室同一时刻只能由一个活动使用。
//每个活动a[i]都有一个开始时间s[i]和结束时间f[i]。一旦被选择后，活动a[i]就占据半开时间区间[s[i], f[i])。
//如果[s[i], f[i])和[s[j], f[j])互不重叠，a[i]和a[j]两个活动就可以被安排在这一天。
//求使得尽量多的活动能不冲突的举行的最大数量。


int main() {
	//test();
	test2();
	return 0;
}
