#pragma once
#include<vector>
#include<bitset>
namespace zhu {
	template<size_t N>
	class BitSet{
	private:
		std::vector<char> _st;
	public:
		BitSet() {
			_st.resize(N / 8 + 1);
		}
		void set(size_t x) {
			size_t i = x / 8; //算出在第几个字节上面
			size_t j = x % 8; //算出在该字节上面第几个位
			_st[i] |= 1 << j; //将该位置1
		}
		void reset(size_t x){
			size_t i = x / 8; //算出在第几个字节上面
			size_t j = x % 8; //算出在该字节上面第几个位
			_st[i] &= ~(1 << j); //将该位置0
		}
		bool test(size_t x) {
			size_t i = x / 8; //算出在第几个字节上面
			size_t j = x % 8; //算出在该字节上面第几个位
			return _st[i] & (1 << j); //测试一下该位是0还是1
		}
	};
	template<size_t N>
	class TwoBitSet {
	public:
		void Set(size_t x) {
			if(!_st1.test(x) && !_st2.test(x)) {// 00->01
				_st2.set(x);
			}
			else if(!_st1.test(x) && _st2.test(x)){ //01->10
				_st1.set(x);
				_st2.reset(x);
			}
			//10->表示已经出现两次或以上不用处理
		}
		void PritnfOnceNum() {
			for (size_t i = 0; i < N; i++) {
				if (!_st1.test(i) && _st2.test(i)) {
					std::cout << i << std::endl;
				}
			}
		}
	private:
		zhu::BitSet<N> _st1;
		zhu::BitSet<N> _st2;

	};
	void test() {
		BitSet<80> bst;
		bst.set(9);
		bst.set(15);
		std::cout << bst.test(9) << std::endl;
		std::cout << bst.test(15) << std::endl;
		bst.reset(9);
		bst.reset(15);
		std::vector<int> v{ 1, 4, 1, 5, 6, 11, 2, 99, 65, 56, 65, 56, 2, 99 };
		TwoBitSet<100> tbs;
		for (auto e : v) {
			tbs.Set(e);
		}
		tbs.PritnfOnceNum();
		//std::bitset<-1> bst1;
		//std::bitset<0xffffffff> bst2;
		//std::bitset<INT_MAX> bst3;
	}
}