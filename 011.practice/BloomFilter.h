#pragma once
//-------------------------布隆过滤器-------------------------/
//适用于	数据量大、节省空间、允许误判这样的场景
#include <bitset>
#include <string>
#include <time.h>
#include<iostream>
//-------------------------一个字符串对应三个位置-------------------/
//三种算法
namespace zhu {
	struct BKDRHash
	{
		size_t operator()(const std::string& s)
		{
			// BKDR
			size_t value = 0;
			for (auto ch : s)
			{
				value *= 31;
				value += ch;
			}
			return value;
		}
	};

	struct APHash
	{
		size_t operator()(const std::string& s)
		{
			size_t hash = 0;
			for (size_t i = 0; i < s.size(); i++)
			{
				if ((i & 1) == 0)
				{
					hash ^= ((hash << 7) ^ s[i] ^ (hash >> 3));
				}
				else
				{
					hash ^= (~((hash << 11) ^ s[i] ^ (hash >> 5)));
				}
			}
			return hash;
		}
	};

	struct DJBHash
	{
		size_t operator()(const std::string& s)
		{
			size_t hash = 5381;
			for (auto ch : s)
			{
				hash += (hash << 5) + ch;
			}
			return hash;
		}
	};

	template<size_t N, size_t X = 12, class K = std::string,
		class HushFunc1 = BKDRHash, class HushFunc2 = APHash, 
		class HushFunc3 = DJBHash>
		class BloomFilter {
		private:
			std::bitset<N* X> _bst;
		public:
			void set(const K& key) {
				size_t len = N * X;
				size_t index1 = HushFunc1()(key) % len;
				size_t index2 = HushFunc2()(key) % len;
				size_t index3 = HushFunc3()(key) % len;
				_bst.set(index1);
				_bst.set(index2);
				_bst.set(index3);
			}
			bool test(const K& key) {
				size_t len = N * X;
				size_t index1 = HushFunc1()(key) % len;
				if (_bst.test(index1) == false)
					return false;
				size_t index2 = HushFunc2()(key) % len;
				if (_bst.test(index2) == false)
					return false;
				size_t index3 = HushFunc3()(key) % len;
				if (_bst.test(index3) == false)
					return false;
				return true;
			}
			// 不支持删除，删除可能会影响其他值。
			void Reset(const K& key);
	};
	void test_BloomFilter() {
		BloomFilter<100> bf;
		bf.set("张三");
		bf.set("李四");
		bf.set("王五");
		bf.set("赵六");
		bf.set("田七");
		bf.set("sdgdfs4");
		std::cout << bf.test("张三") << std::endl;
		std::cout << bf.test("张四") << std::endl;
		std::cout << bf.test("李四") << std::endl;
		std::cout << bf.test("王五") << std::endl;
		std::cout << bf.test("赵六") << std::endl;
		std::cout << bf.test("田七") << std::endl;
		std::cout << bf.test("sdgdfs4") << std::endl;
		std::cout << bf.test("sdgdfs3") << std::endl;

	}
}
